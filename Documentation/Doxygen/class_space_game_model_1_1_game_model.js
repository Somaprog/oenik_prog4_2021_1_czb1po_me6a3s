var class_space_game_model_1_1_game_model =
[
    [ "GameModel", "class_space_game_model_1_1_game_model.html#a00f3e632570d06575680d9bfab4a00fe", null ],
    [ "Asteroids", "class_space_game_model_1_1_game_model.html#a8db997745e3304fd7bc4fb8d5ee52581", null ],
    [ "Bullet", "class_space_game_model_1_1_game_model.html#aa4ce272463af7ffb197a3146c59008b8", null ],
    [ "Coin", "class_space_game_model_1_1_game_model.html#a6f0b4e81feccb6d3cfd01fc185248aa8", null ],
    [ "CoinInventory", "class_space_game_model_1_1_game_model.html#a7e669065426aa4469da452f18de3a27c", null ],
    [ "CurrLvl", "class_space_game_model_1_1_game_model.html#a6a1de7b5fd3039bcbfb46e0e9b51790a", null ],
    [ "CurrPup", "class_space_game_model_1_1_game_model.html#a40558b91f395f2c44a83340f66124c0c", null ],
    [ "GameHeight", "class_space_game_model_1_1_game_model.html#ab7d970d4f6631c3cb08d76b3f20664a3", null ],
    [ "GameWidth", "class_space_game_model_1_1_game_model.html#ae387cbf26a444e1606a1b437a1315730", null ],
    [ "HighScores", "class_space_game_model_1_1_game_model.html#ae5371664924e650d3436b1f5b30861b0", null ],
    [ "Hp", "class_space_game_model_1_1_game_model.html#a286229d207fb3d16d0ed89efa5c03f14", null ],
    [ "PowerUps", "class_space_game_model_1_1_game_model.html#a2f386d1a5b5a282be905aded736e56d5", null ],
    [ "Score", "class_space_game_model_1_1_game_model.html#a0e8cc6c43b0a66bc3c26688eeeeba35b", null ],
    [ "Ship", "class_space_game_model_1_1_game_model.html#a37976957b9c8d77c8b9c3d2eebbf0a05", null ],
    [ "ShipLvl", "class_space_game_model_1_1_game_model.html#a1cb01674c4bc7994516d059291ce486a", null ],
    [ "ShipType", "class_space_game_model_1_1_game_model.html#a021deebdde2985364a2f6ab5bb0fc8fc", null ]
];