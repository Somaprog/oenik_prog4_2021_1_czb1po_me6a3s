var namespaces_dup =
[
    [ "GameControl", "namespace_game_control.html", "namespace_game_control" ],
    [ "SpaceGameControl", "namespace_space_game_control.html", "namespace_space_game_control" ],
    [ "SpaceGameLogic", "namespace_space_game_logic.html", "namespace_space_game_logic" ],
    [ "SpaceGameLogicTests", "namespace_space_game_logic_tests.html", "namespace_space_game_logic_tests" ],
    [ "SpaceGameModel", "namespace_space_game_model.html", "namespace_space_game_model" ],
    [ "SpaceGameRenderer", "namespace_space_game_renderer.html", "namespace_space_game_renderer" ],
    [ "SpaceGameRepository", "namespace_space_game_repository.html", "namespace_space_game_repository" ],
    [ "XamlGeneratedNamespace", "namespace_xaml_generated_namespace.html", "namespace_xaml_generated_namespace" ]
];