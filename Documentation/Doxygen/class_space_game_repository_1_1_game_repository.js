var class_space_game_repository_1_1_game_repository =
[
    [ "DeleteGame", "class_space_game_repository_1_1_game_repository.html#ad145189beddac822af72f9845cc8e60f", null ],
    [ "HighScoreSave", "class_space_game_repository_1_1_game_repository.html#a0dc56fbff65882227687ed4fa76e7ff6", null ],
    [ "LoadGame", "class_space_game_repository_1_1_game_repository.html#a362ea8cb7e008590ec4b483a00294f96", null ],
    [ "LoadHighScore", "class_space_game_repository_1_1_game_repository.html#afb6bcc30bdf866c20f5b8e19bd1d90be", null ],
    [ "SaveGame", "class_space_game_repository_1_1_game_repository.html#a73235f260cf07fd48e1ae87721ed1be5", null ]
];