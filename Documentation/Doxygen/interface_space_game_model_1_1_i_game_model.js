var interface_space_game_model_1_1_i_game_model =
[
    [ "Asteroids", "interface_space_game_model_1_1_i_game_model.html#a6c73d90f2a0e9cc56cc1b7f22d736aac", null ],
    [ "Bullet", "interface_space_game_model_1_1_i_game_model.html#af3fe463d198920b9b7c64bc94b9a3344", null ],
    [ "Coin", "interface_space_game_model_1_1_i_game_model.html#a0ac2bd601d45e1419623adac0ca46d67", null ],
    [ "CoinInventory", "interface_space_game_model_1_1_i_game_model.html#a25ad3065f3a1b64eac958afbc239345a", null ],
    [ "CurrLvl", "interface_space_game_model_1_1_i_game_model.html#a59364764d701b04e7ac6102636f5fd35", null ],
    [ "CurrPup", "interface_space_game_model_1_1_i_game_model.html#a9002a95d3bb5bd080e8be5d04fe0f17c", null ],
    [ "GameHeight", "interface_space_game_model_1_1_i_game_model.html#a949c2e9fc52c73a903a92bbfdc841722", null ],
    [ "GameWidth", "interface_space_game_model_1_1_i_game_model.html#a9444528302e042b8f274d02867d55d43", null ],
    [ "HighScores", "interface_space_game_model_1_1_i_game_model.html#a3b6ee26b1c8a3444a59f8ca92adeaa72", null ],
    [ "Hp", "interface_space_game_model_1_1_i_game_model.html#ad5a93610325579df600898317ad7483d", null ],
    [ "PowerUps", "interface_space_game_model_1_1_i_game_model.html#ac941ac8d5156182db8884d63706282d7", null ],
    [ "Score", "interface_space_game_model_1_1_i_game_model.html#a46e610c620d24153dd64e2b1c2091bde", null ],
    [ "Ship", "interface_space_game_model_1_1_i_game_model.html#a93186cc789b8a4032e9dbcc3b57a7bac", null ],
    [ "ShipLvl", "interface_space_game_model_1_1_i_game_model.html#a53bd1dc4f6da989da4f9cdaadb51cc5b", null ],
    [ "ShipType", "interface_space_game_model_1_1_i_game_model.html#a820058bfa720957fc5702ab14083de63", null ]
];