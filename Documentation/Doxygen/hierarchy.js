var hierarchy =
[
    [ "Application", null, [
      [ "GameControl.App", "class_game_control_1_1_app.html", null ]
    ] ],
    [ "System.Windows.Application", null, [
      [ "GameControl.App", "class_game_control_1_1_app.html", null ],
      [ "GameControl.App", "class_game_control_1_1_app.html", null ],
      [ "GameControl.App", "class_game_control_1_1_app.html", null ],
      [ "GameControl.App", "class_game_control_1_1_app.html", null ],
      [ "GameControl.App", "class_game_control_1_1_app.html", null ]
    ] ],
    [ "FrameworkElement", null, [
      [ "SpaceGameControl.Control", "class_space_game_control_1_1_control.html", null ]
    ] ],
    [ "SpaceGameModel.GameInfo", "class_space_game_model_1_1_game_info.html", null ],
    [ "SpaceGameRenderer.GameRenderer", "class_space_game_renderer_1_1_game_renderer.html", null ],
    [ "SpaceGameModel.HighScore", "class_space_game_model_1_1_high_score.html", null ],
    [ "System.Windows.Markup.IComponentConnector", null, [
      [ "SpaceGameControl.MainWindow", "class_space_game_control_1_1_main_window.html", null ],
      [ "SpaceGameControl.MainWindow", "class_space_game_control_1_1_main_window.html", null ],
      [ "SpaceGameControl.MainWindow", "class_space_game_control_1_1_main_window.html", null ],
      [ "SpaceGameControl.MainWindow", "class_space_game_control_1_1_main_window.html", null ],
      [ "SpaceGameControl.MainWindow", "class_space_game_control_1_1_main_window.html", null ]
    ] ],
    [ "SpaceGameModel.IGameItem", "interface_space_game_model_1_1_i_game_item.html", [
      [ "SpaceGameModel.GameItem", "class_space_game_model_1_1_game_item.html", [
        [ "SpaceGameModel.AsteroidItem", "class_space_game_model_1_1_asteroid_item.html", null ],
        [ "SpaceGameModel.BulletItem", "class_space_game_model_1_1_bullet_item.html", null ],
        [ "SpaceGameModel.CoinItem", "class_space_game_model_1_1_coin_item.html", null ],
        [ "SpaceGameModel.HealthItem", "class_space_game_model_1_1_health_item.html", null ],
        [ "SpaceGameModel.PowerUpItem", "class_space_game_model_1_1_power_up_item.html", null ],
        [ "SpaceGameModel.ShipItem", "class_space_game_model_1_1_ship_item.html", null ]
      ] ]
    ] ],
    [ "SpaceGameLogic.IGameLogic", "interface_space_game_logic_1_1_i_game_logic.html", [
      [ "SpaceGameLogic.GameLogic", "class_space_game_logic_1_1_game_logic.html", null ]
    ] ],
    [ "SpaceGameModel.IGameModel", "interface_space_game_model_1_1_i_game_model.html", [
      [ "SpaceGameModel.GameModel", "class_space_game_model_1_1_game_model.html", null ]
    ] ],
    [ "SpaceGameRepository.IGameRepository", "interface_space_game_repository_1_1_i_game_repository.html", [
      [ "SpaceGameRepository.GameRepository", "class_space_game_repository_1_1_game_repository.html", null ]
    ] ],
    [ "System.Windows.Markup.InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ],
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "SpaceGameLogicTests.LogicTests", "class_space_game_logic_tests_1_1_logic_tests.html", null ],
    [ "System.Windows.Window", null, [
      [ "SpaceGameControl.MainWindow", "class_space_game_control_1_1_main_window.html", null ],
      [ "SpaceGameControl.MainWindow", "class_space_game_control_1_1_main_window.html", null ],
      [ "SpaceGameControl.MainWindow", "class_space_game_control_1_1_main_window.html", null ],
      [ "SpaceGameControl.MainWindow", "class_space_game_control_1_1_main_window.html", null ],
      [ "SpaceGameControl.MainWindow", "class_space_game_control_1_1_main_window.html", null ]
    ] ],
    [ "Window", null, [
      [ "SpaceGameControl.MainWindow", "class_space_game_control_1_1_main_window.html", null ]
    ] ]
];