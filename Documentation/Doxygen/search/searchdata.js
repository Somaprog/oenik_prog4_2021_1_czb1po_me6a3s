var indexSectionsWithContent =
{
  0: ".abcdghilmnoprsux",
  1: "abcghilmps",
  2: "gsx",
  3: ".abcghilmprs",
  4: "abcdghilmopsu",
  5: "abcdghinoprs"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "properties"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Properties"
};

