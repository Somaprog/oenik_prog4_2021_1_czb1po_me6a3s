var searchData=
[
  ['gameinfo_270',['GameInfo',['../class_space_game_model_1_1_game_info.html#a3e4af8b197ba310c90edc422101b9664',1,'SpaceGameModel::GameInfo']]],
  ['gamelogic_271',['GameLogic',['../class_space_game_logic_1_1_game_logic.html#a17878db79eb6b1633ad7e668603f9478',1,'SpaceGameLogic.GameLogic.GameLogic(IGameModel model)'],['../class_space_game_logic_1_1_game_logic.html#a386b626a76603ef16e960065e2767bb0',1,'SpaceGameLogic.GameLogic.GameLogic(IGameModel model, IGameRepository repo)'],['../class_space_game_logic_1_1_game_logic.html#a98e0711ac6774fef7e0b1c0610b28d1d',1,'SpaceGameLogic.GameLogic.GameLogic(double w, double h)']]],
  ['gamelogictest_272',['GameLogicTest',['../class_space_game_logic_tests_1_1_logic_tests.html#a85ad83f7ef9c857f200ffd7b509e7414',1,'SpaceGameLogicTests::LogicTests']]],
  ['gamelogictest1_273',['GameLogicTest1',['../class_space_game_logic_tests_1_1_logic_tests.html#a72109770afea9434da38e69422c1691c',1,'SpaceGameLogicTests::LogicTests']]],
  ['gamemodel_274',['GameModel',['../class_space_game_model_1_1_game_model.html#a00f3e632570d06575680d9bfab4a00fe',1,'SpaceGameModel::GameModel']]],
  ['gameover_275',['GameOver',['../class_space_game_logic_1_1_game_logic.html#af8ffaf03ed1a28dc0bdd43e1632d376d',1,'SpaceGameLogic.GameLogic.GameOver()'],['../interface_space_game_logic_1_1_i_game_logic.html#aa3bfab92fc0f7ed781e06b6600d19360',1,'SpaceGameLogic.IGameLogic.GameOver()']]],
  ['gameovertest_276',['GameOverTest',['../class_space_game_logic_tests_1_1_logic_tests.html#a8da22e9bdcfe9d5140733622912f6055',1,'SpaceGameLogicTests::LogicTests']]],
  ['gamerenderer_277',['GameRenderer',['../class_space_game_renderer_1_1_game_renderer.html#a323d5d2252521a4d0bcaef946f0bc7e6',1,'SpaceGameRenderer::GameRenderer']]],
  ['gethashcode_278',['GetHashCode',['../class_space_game_model_1_1_game_info.html#af5964c959ed9e931aa25ef896f8475fd',1,'SpaceGameModel::GameInfo']]],
  ['getpropertyvalue_279',['GetPropertyValue',['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#afdc9fe15b56607d02082908d934480c6',1,'XamlGeneratedNamespace.GeneratedInternalTypeHelper.GetPropertyValue(System.Reflection.PropertyInfo propertyInfo, object target, System.Globalization.CultureInfo culture)'],['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#afdc9fe15b56607d02082908d934480c6',1,'XamlGeneratedNamespace.GeneratedInternalTypeHelper.GetPropertyValue(System.Reflection.PropertyInfo propertyInfo, object target, System.Globalization.CultureInfo culture)']]]
];
