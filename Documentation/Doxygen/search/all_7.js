var searchData=
[
  ['id_94',['ID',['../class_space_game_model_1_1_power_up_item.html#a14f59eeeead6bae5d6a3d45a43887ce5',1,'SpaceGameModel::PowerUpItem']]],
  ['igameitem_95',['IGameItem',['../interface_space_game_model_1_1_i_game_item.html',1,'SpaceGameModel']]],
  ['igameitem_2ecs_96',['IGameItem.cs',['../_i_game_item_8cs.html',1,'']]],
  ['igamelogic_97',['IGameLogic',['../interface_space_game_logic_1_1_i_game_logic.html',1,'SpaceGameLogic']]],
  ['igamelogic_2ecs_98',['IGameLogic.cs',['../_i_game_logic_8cs.html',1,'']]],
  ['igamemodel_99',['IGameModel',['../interface_space_game_model_1_1_i_game_model.html',1,'SpaceGameModel']]],
  ['igamemodel_2ecs_100',['IGameModel.cs',['../_i_game_model_8cs.html',1,'']]],
  ['igamerepository_101',['IGameRepository',['../interface_space_game_repository_1_1_i_game_repository.html',1,'SpaceGameRepository']]],
  ['igamerepository_2ecs_102',['IGameRepository.cs',['../_i_game_repository_8cs.html',1,'']]],
  ['initializecomponent_103',['InitializeComponent',['../class_game_control_1_1_app.html#a5d3fb2ec0937e8fab1918443207cfc24',1,'GameControl.App.InitializeComponent()'],['../class_game_control_1_1_app.html#a5d3fb2ec0937e8fab1918443207cfc24',1,'GameControl.App.InitializeComponent()'],['../class_space_game_control_1_1_main_window.html#a7ea1ae9a67792fc2b755da7a7eaf80be',1,'SpaceGameControl.MainWindow.InitializeComponent()'],['../class_space_game_control_1_1_main_window.html#a7ea1ae9a67792fc2b755da7a7eaf80be',1,'SpaceGameControl.MainWindow.InitializeComponent()'],['../class_game_control_1_1_app.html#a5d3fb2ec0937e8fab1918443207cfc24',1,'GameControl.App.InitializeComponent()'],['../class_space_game_control_1_1_main_window.html#a7ea1ae9a67792fc2b755da7a7eaf80be',1,'SpaceGameControl.MainWindow.InitializeComponent()'],['../class_game_control_1_1_app.html#a5d3fb2ec0937e8fab1918443207cfc24',1,'GameControl.App.InitializeComponent()'],['../class_game_control_1_1_app.html#a5d3fb2ec0937e8fab1918443207cfc24',1,'GameControl.App.InitializeComponent()'],['../class_space_game_control_1_1_main_window.html#a7ea1ae9a67792fc2b755da7a7eaf80be',1,'SpaceGameControl.MainWindow.InitializeComponent()'],['../class_space_game_control_1_1_main_window.html#a7ea1ae9a67792fc2b755da7a7eaf80be',1,'SpaceGameControl.MainWindow.InitializeComponent()']]],
  ['isactive_104',['IsActive',['../class_space_game_model_1_1_health_item.html#a49923e972634e528239c98bfdf9ca4bc',1,'SpaceGameModel::HealthItem']]],
  ['iscollision_105',['IsCollision',['../class_space_game_model_1_1_game_item.html#a4d56b054c5dfd15510a5d270ddbd0b43',1,'SpaceGameModel.GameItem.IsCollision()'],['../interface_space_game_model_1_1_i_game_item.html#a8ff674c71bffb4bd313c498244976b39',1,'SpaceGameModel.IGameItem.IsCollision()']]]
];
