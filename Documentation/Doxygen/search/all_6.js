var searchData=
[
  ['healthitem_85',['HealthItem',['../class_space_game_model_1_1_health_item.html#ababcd2c2ea534d06aec92b0034e7de25',1,'SpaceGameModel.HealthItem.HealthItem()'],['../class_space_game_model_1_1_health_item.html',1,'SpaceGameModel.HealthItem']]],
  ['healthitem_2ecs_86',['HealthItem.cs',['../_health_item_8cs.html',1,'']]],
  ['highscore_87',['HighScore',['../class_space_game_model_1_1_high_score.html',1,'SpaceGameModel']]],
  ['highscore_2ecs_88',['HighScore.cs',['../_high_score_8cs.html',1,'']]],
  ['highscores_89',['HighScores',['../class_space_game_model_1_1_game_model.html#ae5371664924e650d3436b1f5b30861b0',1,'SpaceGameModel.GameModel.HighScores()'],['../interface_space_game_model_1_1_i_game_model.html#a3b6ee26b1c8a3444a59f8ca92adeaa72',1,'SpaceGameModel.IGameModel.HighScores()']]],
  ['highscoresave_90',['HighScoreSave',['../class_space_game_repository_1_1_game_repository.html#a0dc56fbff65882227687ed4fa76e7ff6',1,'SpaceGameRepository.GameRepository.HighScoreSave()'],['../interface_space_game_repository_1_1_i_game_repository.html#a85cfdf7fb45df813ae3064fd6200fa1c',1,'SpaceGameRepository.IGameRepository.HighScoreSave()']]],
  ['hp_91',['Hp',['../class_space_game_model_1_1_game_info.html#a062d0f8fbf37680279c418d6a4c0a6a8',1,'SpaceGameModel.GameInfo.Hp()'],['../class_space_game_model_1_1_game_model.html#a286229d207fb3d16d0ed89efa5c03f14',1,'SpaceGameModel.GameModel.Hp()'],['../interface_space_game_model_1_1_i_game_model.html#ad5a93610325579df600898317ad7483d',1,'SpaceGameModel.IGameModel.Hp()']]],
  ['hpup_92',['HpUp',['../class_space_game_logic_1_1_game_logic.html#a89c17df6475895fe5554928c52ab92c2',1,'SpaceGameLogic.GameLogic.HpUp()'],['../interface_space_game_logic_1_1_i_game_logic.html#a8fc8cc7b9370869fdd85a18bdfd00364',1,'SpaceGameLogic.IGameLogic.HpUp()']]],
  ['hpuptest_93',['HpUpTest',['../class_space_game_logic_tests_1_1_logic_tests.html#a5d73f1a54af74a1f7e2498b93bd7acd2',1,'SpaceGameLogicTests::LogicTests']]]
];
