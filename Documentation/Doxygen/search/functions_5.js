var searchData=
[
  ['healthitem_280',['HealthItem',['../class_space_game_model_1_1_health_item.html#ababcd2c2ea534d06aec92b0034e7de25',1,'SpaceGameModel::HealthItem']]],
  ['highscoresave_281',['HighScoreSave',['../class_space_game_repository_1_1_game_repository.html#a0dc56fbff65882227687ed4fa76e7ff6',1,'SpaceGameRepository.GameRepository.HighScoreSave()'],['../interface_space_game_repository_1_1_i_game_repository.html#a85cfdf7fb45df813ae3064fd6200fa1c',1,'SpaceGameRepository.IGameRepository.HighScoreSave()']]],
  ['hpup_282',['HpUp',['../class_space_game_logic_1_1_game_logic.html#a89c17df6475895fe5554928c52ab92c2',1,'SpaceGameLogic.GameLogic.HpUp()'],['../interface_space_game_logic_1_1_i_game_logic.html#a8fc8cc7b9370869fdd85a18bdfd00364',1,'SpaceGameLogic.IGameLogic.HpUp()']]],
  ['hpuptest_283',['HpUpTest',['../class_space_game_logic_tests_1_1_logic_tests.html#a5d73f1a54af74a1f7e2498b93bd7acd2',1,'SpaceGameLogicTests::LogicTests']]]
];
