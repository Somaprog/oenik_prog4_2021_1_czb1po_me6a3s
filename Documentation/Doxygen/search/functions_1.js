var searchData=
[
  ['buildgamedisplay_243',['BuildGameDisplay',['../class_space_game_renderer_1_1_game_renderer.html#af6832de9c7886c5c8730c60813a5a39b',1,'SpaceGameRenderer.GameRenderer.BuildGameDisplay(DrawingContext ctx)'],['../class_space_game_renderer_1_1_game_renderer.html#a651b03c05e73b25a464b1e3e438eebc6',1,'SpaceGameRenderer.GameRenderer.BuildGameDisplay(DrawingContext ctx, int coin, int currLvl, int currPUp, int score, int shipLvl, int shipType)']]],
  ['bulletitem_244',['BulletItem',['../class_space_game_model_1_1_bullet_item.html#a6adabf2931333fd93a1835e7dad681e8',1,'SpaceGameModel::BulletItem']]],
  ['bulletmove_245',['BulletMove',['../class_space_game_logic_1_1_game_logic.html#a129e01006693eab0f39579d36f0372c2',1,'SpaceGameLogic.GameLogic.BulletMove()'],['../interface_space_game_logic_1_1_i_game_logic.html#ad20f86b0ba19279e37c65a61106ab245',1,'SpaceGameLogic.IGameLogic.BulletMove()']]]
];
