var searchData=
[
  ['loadgame_106',['LoadGame',['../class_space_game_repository_1_1_game_repository.html#a362ea8cb7e008590ec4b483a00294f96',1,'SpaceGameRepository.GameRepository.LoadGame()'],['../interface_space_game_repository_1_1_i_game_repository.html#ae9ae6b459953fcb9a32ca7d56615a45a',1,'SpaceGameRepository.IGameRepository.LoadGame()']]],
  ['loadhighscore_107',['LoadHighScore',['../class_space_game_repository_1_1_game_repository.html#afb6bcc30bdf866c20f5b8e19bd1d90be',1,'SpaceGameRepository.GameRepository.LoadHighScore()'],['../interface_space_game_repository_1_1_i_game_repository.html#a0ca0199ead6b56e18f5a5195650fc772',1,'SpaceGameRepository.IGameRepository.LoadHighScore()']]],
  ['loadhighscores_108',['LoadHighscores',['../class_space_game_logic_1_1_game_logic.html#a0a9aa319307de9d908a50e77bdec6a94',1,'SpaceGameLogic.GameLogic.LoadHighscores()'],['../interface_space_game_logic_1_1_i_game_logic.html#ab91604ff8154ce6d2c391ca53f6dc127',1,'SpaceGameLogic.IGameLogic.LoadHighscores()']]],
  ['loadhighscorestest_109',['LoadHighScoresTest',['../class_space_game_logic_tests_1_1_logic_tests.html#ad3d64b81c1948b476f208b44596cbbb5',1,'SpaceGameLogicTests::LogicTests']]],
  ['loadsave_110',['LoadSave',['../class_space_game_logic_1_1_game_logic.html#af6f80d01e891bb604f075613da85c424',1,'SpaceGameLogic.GameLogic.LoadSave()'],['../interface_space_game_logic_1_1_i_game_logic.html#a41d5313293713ea21d62dd866215bcde',1,'SpaceGameLogic.IGameLogic.LoadSave()']]],
  ['logictests_111',['LogicTests',['../class_space_game_logic_tests_1_1_logic_tests.html',1,'SpaceGameLogicTests']]],
  ['logictests_2ecs_112',['LogicTests.cs',['../_logic_tests_8cs.html',1,'']]]
];
