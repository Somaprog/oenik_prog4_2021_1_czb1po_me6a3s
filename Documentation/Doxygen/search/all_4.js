var searchData=
[
  ['deletegame_42',['DeleteGame',['../class_space_game_repository_1_1_game_repository.html#ad145189beddac822af72f9845cc8e60f',1,'SpaceGameRepository.GameRepository.DeleteGame()'],['../interface_space_game_repository_1_1_i_game_repository.html#af984aa2a7b5d97ba2d9cd09eae895abb',1,'SpaceGameRepository.IGameRepository.DeleteGame()']]],
  ['deletesave_43',['DeleteSave',['../class_space_game_logic_1_1_game_logic.html#a6488a499e0a7749b73e249078448cdb1',1,'SpaceGameLogic.GameLogic.DeleteSave()'],['../interface_space_game_logic_1_1_i_game_logic.html#ae02ab797ffff7d6cada86c1bedb0d070',1,'SpaceGameLogic.IGameLogic.DeleteSave()']]],
  ['deletesavetest_44',['DeleteSaveTest',['../class_space_game_logic_tests_1_1_logic_tests.html#a09a21f03edcfdb9e10c1972965346e11',1,'SpaceGameLogicTests::LogicTests']]],
  ['drawbullet_45',['DrawBullet',['../class_space_game_renderer_1_1_game_renderer.html#a097aa478995845b5aa47971d86becec2',1,'SpaceGameRenderer::GameRenderer']]],
  ['drawgameover_46',['DrawGameOver',['../class_space_game_renderer_1_1_game_renderer.html#ab9b5006a16bfc2a41a0c3b9d7c40452d',1,'SpaceGameRenderer::GameRenderer']]],
  ['drawhsmenu_47',['DrawHSMenu',['../class_space_game_renderer_1_1_game_renderer.html#a91015caf266cefb2e1fb05d99366261d',1,'SpaceGameRenderer::GameRenderer']]],
  ['drawmainmenu_48',['DrawMainMenu',['../class_space_game_renderer_1_1_game_renderer.html#aad7bbeee3b180b68cf0ef10e1eb233b5',1,'SpaceGameRenderer::GameRenderer']]],
  ['drawpausemenu_49',['DrawPauseMenu',['../class_space_game_renderer_1_1_game_renderer.html#ac8d360203dddf4255b4633931acb69c6',1,'SpaceGameRenderer::GameRenderer']]],
  ['drawpup_50',['DrawPUp',['../class_space_game_renderer_1_1_game_renderer.html#aaf4493dce649a7ae1a9d64c7b6191876',1,'SpaceGameRenderer::GameRenderer']]],
  ['drawskinshop_51',['DrawSkinShop',['../class_space_game_renderer_1_1_game_renderer.html#a797a31407f2fda69ebb5d742af97f0c8',1,'SpaceGameRenderer::GameRenderer']]],
  ['dx_52',['DX',['../class_space_game_model_1_1_bullet_item.html#ae7359b37d9ff96ae40929b9d7f9d2de4',1,'SpaceGameModel.BulletItem.DX()'],['../class_space_game_model_1_1_ship_item.html#aa3b0d233f19a2f33a36d2ac6f710b2fa',1,'SpaceGameModel.ShipItem.DX()']]],
  ['dy_53',['DY',['../class_space_game_model_1_1_bullet_item.html#a5679d6fdbf03582bfa3d45b3b4cdaf77',1,'SpaceGameModel.BulletItem.DY()'],['../class_space_game_model_1_1_ship_item.html#a9e568f726200836bf3b7152a3f8b4c06',1,'SpaceGameModel.ShipItem.DY()']]]
];
