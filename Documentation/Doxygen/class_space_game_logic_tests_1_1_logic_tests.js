var class_space_game_logic_tests_1_1_logic_tests =
[
    [ "AutoSaveTest", "class_space_game_logic_tests_1_1_logic_tests.html#af8c782375ef86c7b1b01c9e95c18ecd8", null ],
    [ "ChangeLvlTest", "class_space_game_logic_tests_1_1_logic_tests.html#a17efa5208a01d76a4946d379692055d2", null ],
    [ "ChooseShipTest", "class_space_game_logic_tests_1_1_logic_tests.html#a16be25938f9f0ebc776de484d18e24c8", null ],
    [ "CoinUpTest", "class_space_game_logic_tests_1_1_logic_tests.html#a47a14af09df6eccf58f490a511c1e897", null ],
    [ "CollidedTest", "class_space_game_logic_tests_1_1_logic_tests.html#a7787248461ef548dd66aed805da253c6", null ],
    [ "DeleteSaveTest", "class_space_game_logic_tests_1_1_logic_tests.html#a09a21f03edcfdb9e10c1972965346e11", null ],
    [ "GameLogicTest", "class_space_game_logic_tests_1_1_logic_tests.html#a85ad83f7ef9c857f200ffd7b509e7414", null ],
    [ "GameLogicTest1", "class_space_game_logic_tests_1_1_logic_tests.html#a72109770afea9434da38e69422c1691c", null ],
    [ "GameOverTest", "class_space_game_logic_tests_1_1_logic_tests.html#a8da22e9bdcfe9d5140733622912f6055", null ],
    [ "HpUpTest", "class_space_game_logic_tests_1_1_logic_tests.html#a5d73f1a54af74a1f7e2498b93bd7acd2", null ],
    [ "LoadHighScoresTest", "class_space_game_logic_tests_1_1_logic_tests.html#ad3d64b81c1948b476f208b44596cbbb5", null ],
    [ "MoveTest", "class_space_game_logic_tests_1_1_logic_tests.html#ad48c832a58e33552834525bfd8ddab89", null ],
    [ "PickPUpTest", "class_space_game_logic_tests_1_1_logic_tests.html#aef25cdccad9cd78d0e2771cf7298b2ed", null ],
    [ "ScoreUpTest", "class_space_game_logic_tests_1_1_logic_tests.html#a76f39779e9058f32546dd02db5b26f7f", null ],
    [ "SetUpUnitTests", "class_space_game_logic_tests_1_1_logic_tests.html#a7f2d9f312c71668c4a64989d0ae7a8a7", null ],
    [ "ShipUpTest", "class_space_game_logic_tests_1_1_logic_tests.html#aca8b6f48706354ebb8eee2b550c3384c", null ],
    [ "UsePowerUpTest", "class_space_game_logic_tests_1_1_logic_tests.html#a4aeecdff97555976d659880feb0bf549", null ]
];