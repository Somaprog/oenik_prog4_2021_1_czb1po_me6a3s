var annotated_dup =
[
    [ "GameControl", "namespace_game_control.html", [
      [ "App", "class_game_control_1_1_app.html", "class_game_control_1_1_app" ]
    ] ],
    [ "SpaceGameControl", "namespace_space_game_control.html", [
      [ "Control", "class_space_game_control_1_1_control.html", "class_space_game_control_1_1_control" ],
      [ "MainWindow", "class_space_game_control_1_1_main_window.html", "class_space_game_control_1_1_main_window" ]
    ] ],
    [ "SpaceGameLogic", "namespace_space_game_logic.html", [
      [ "GameLogic", "class_space_game_logic_1_1_game_logic.html", "class_space_game_logic_1_1_game_logic" ],
      [ "IGameLogic", "interface_space_game_logic_1_1_i_game_logic.html", "interface_space_game_logic_1_1_i_game_logic" ]
    ] ],
    [ "SpaceGameLogicTests", "namespace_space_game_logic_tests.html", [
      [ "LogicTests", "class_space_game_logic_tests_1_1_logic_tests.html", "class_space_game_logic_tests_1_1_logic_tests" ]
    ] ],
    [ "SpaceGameModel", "namespace_space_game_model.html", [
      [ "GameInfo", "class_space_game_model_1_1_game_info.html", "class_space_game_model_1_1_game_info" ],
      [ "AsteroidItem", "class_space_game_model_1_1_asteroid_item.html", "class_space_game_model_1_1_asteroid_item" ],
      [ "BulletItem", "class_space_game_model_1_1_bullet_item.html", "class_space_game_model_1_1_bullet_item" ],
      [ "CoinItem", "class_space_game_model_1_1_coin_item.html", "class_space_game_model_1_1_coin_item" ],
      [ "GameItem", "class_space_game_model_1_1_game_item.html", "class_space_game_model_1_1_game_item" ],
      [ "HealthItem", "class_space_game_model_1_1_health_item.html", "class_space_game_model_1_1_health_item" ],
      [ "IGameItem", "interface_space_game_model_1_1_i_game_item.html", "interface_space_game_model_1_1_i_game_item" ],
      [ "PowerUpItem", "class_space_game_model_1_1_power_up_item.html", "class_space_game_model_1_1_power_up_item" ],
      [ "ShipItem", "class_space_game_model_1_1_ship_item.html", "class_space_game_model_1_1_ship_item" ],
      [ "GameModel", "class_space_game_model_1_1_game_model.html", "class_space_game_model_1_1_game_model" ],
      [ "HighScore", "class_space_game_model_1_1_high_score.html", "class_space_game_model_1_1_high_score" ],
      [ "IGameModel", "interface_space_game_model_1_1_i_game_model.html", "interface_space_game_model_1_1_i_game_model" ]
    ] ],
    [ "SpaceGameRenderer", "namespace_space_game_renderer.html", [
      [ "GameRenderer", "class_space_game_renderer_1_1_game_renderer.html", "class_space_game_renderer_1_1_game_renderer" ]
    ] ],
    [ "SpaceGameRepository", "namespace_space_game_repository.html", [
      [ "GameRepository", "class_space_game_repository_1_1_game_repository.html", "class_space_game_repository_1_1_game_repository" ],
      [ "IGameRepository", "interface_space_game_repository_1_1_i_game_repository.html", "interface_space_game_repository_1_1_i_game_repository" ]
    ] ],
    [ "XamlGeneratedNamespace", "namespace_xaml_generated_namespace.html", [
      [ "GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", "class_xaml_generated_namespace_1_1_generated_internal_type_helper" ]
    ] ]
];