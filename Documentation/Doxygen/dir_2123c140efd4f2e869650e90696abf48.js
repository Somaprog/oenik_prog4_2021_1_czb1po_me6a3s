var dir_2123c140efd4f2e869650e90696abf48 =
[
    [ "AsteroidItem.cs", "_asteroid_item_8cs.html", [
      [ "AsteroidItem", "class_space_game_model_1_1_asteroid_item.html", "class_space_game_model_1_1_asteroid_item" ]
    ] ],
    [ "BulletItem.cs", "_bullet_item_8cs.html", [
      [ "BulletItem", "class_space_game_model_1_1_bullet_item.html", "class_space_game_model_1_1_bullet_item" ]
    ] ],
    [ "CoinItem.cs", "_coin_item_8cs.html", [
      [ "CoinItem", "class_space_game_model_1_1_coin_item.html", "class_space_game_model_1_1_coin_item" ]
    ] ],
    [ "GameItem.cs", "_game_item_8cs.html", [
      [ "GameItem", "class_space_game_model_1_1_game_item.html", "class_space_game_model_1_1_game_item" ]
    ] ],
    [ "HealthItem.cs", "_health_item_8cs.html", [
      [ "HealthItem", "class_space_game_model_1_1_health_item.html", "class_space_game_model_1_1_health_item" ]
    ] ],
    [ "IGameItem.cs", "_i_game_item_8cs.html", [
      [ "IGameItem", "interface_space_game_model_1_1_i_game_item.html", "interface_space_game_model_1_1_i_game_item" ]
    ] ],
    [ "PowerUpItem.cs", "_power_up_item_8cs.html", [
      [ "PowerUpItem", "class_space_game_model_1_1_power_up_item.html", "class_space_game_model_1_1_power_up_item" ]
    ] ],
    [ "ShipItem.cs", "_ship_item_8cs.html", [
      [ "ShipItem", "class_space_game_model_1_1_ship_item.html", "class_space_game_model_1_1_ship_item" ]
    ] ]
];