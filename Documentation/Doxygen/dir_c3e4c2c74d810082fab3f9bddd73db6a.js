var dir_c3e4c2c74d810082fab3f9bddd73db6a =
[
    [ "GameItems", "dir_2123c140efd4f2e869650e90696abf48.html", "dir_2123c140efd4f2e869650e90696abf48" ],
    [ "obj", "dir_21b42636e2770d5721af75b56d5c819d.html", "dir_21b42636e2770d5721af75b56d5c819d" ],
    [ "GameInfo.cs", "_game_info_8cs.html", [
      [ "GameInfo", "class_space_game_model_1_1_game_info.html", "class_space_game_model_1_1_game_info" ]
    ] ],
    [ "GameModel.cs", "_game_model_8cs.html", [
      [ "GameModel", "class_space_game_model_1_1_game_model.html", "class_space_game_model_1_1_game_model" ]
    ] ],
    [ "GlobalSuppressions.cs", "_game_model_2_global_suppressions_8cs.html", null ],
    [ "HighScore.cs", "_high_score_8cs.html", [
      [ "HighScore", "class_space_game_model_1_1_high_score.html", "class_space_game_model_1_1_high_score" ]
    ] ],
    [ "IGameModel.cs", "_i_game_model_8cs.html", [
      [ "IGameModel", "interface_space_game_model_1_1_i_game_model.html", "interface_space_game_model_1_1_i_game_model" ]
    ] ]
];