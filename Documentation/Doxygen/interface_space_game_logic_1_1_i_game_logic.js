var interface_space_game_logic_1_1_i_game_logic =
[
    [ "AsteroidMove", "interface_space_game_logic_1_1_i_game_logic.html#a734192c455e14bc83a3ec3b6e1f97b4e", null ],
    [ "AutoSave", "interface_space_game_logic_1_1_i_game_logic.html#a5864ecea197bc2e8744af808baeb0fb8", null ],
    [ "BulletMove", "interface_space_game_logic_1_1_i_game_logic.html#ad20f86b0ba19279e37c65a61106ab245", null ],
    [ "ChangeLvl", "interface_space_game_logic_1_1_i_game_logic.html#a2da50e9311a0c3432b594118862037bd", null ],
    [ "CheckCollide", "interface_space_game_logic_1_1_i_game_logic.html#a0abc3bee730e105d203810a54efa45c5", null ],
    [ "ChooseShip", "interface_space_game_logic_1_1_i_game_logic.html#a9863d6b88c4cbc69378af4108fbac6ed", null ],
    [ "CoinMove", "interface_space_game_logic_1_1_i_game_logic.html#ad5f03a3dd8bd84585186aa415410eb57", null ],
    [ "CoinUp", "interface_space_game_logic_1_1_i_game_logic.html#a8ded2f6e98da4c5bfda88e27478ac029", null ],
    [ "Collided", "interface_space_game_logic_1_1_i_game_logic.html#aa5f8e386d249588dad32a3485ba83290", null ],
    [ "DeleteSave", "interface_space_game_logic_1_1_i_game_logic.html#ae02ab797ffff7d6cada86c1bedb0d070", null ],
    [ "GameOver", "interface_space_game_logic_1_1_i_game_logic.html#aa3bfab92fc0f7ed781e06b6600d19360", null ],
    [ "HpUp", "interface_space_game_logic_1_1_i_game_logic.html#a8fc8cc7b9370869fdd85a18bdfd00364", null ],
    [ "LoadHighscores", "interface_space_game_logic_1_1_i_game_logic.html#ab91604ff8154ce6d2c391ca53f6dc127", null ],
    [ "LoadSave", "interface_space_game_logic_1_1_i_game_logic.html#a41d5313293713ea21d62dd866215bcde", null ],
    [ "MoveShip", "interface_space_game_logic_1_1_i_game_logic.html#a9c41615299491874a49598597caa4980", null ],
    [ "PickPUp", "interface_space_game_logic_1_1_i_game_logic.html#a22ff867c74e80d95af5df31ebaf6d4e4", null ],
    [ "PUpMove", "interface_space_game_logic_1_1_i_game_logic.html#a3202ca886b335fbc9f070d9ca99f4329", null ],
    [ "ScoreUp", "interface_space_game_logic_1_1_i_game_logic.html#a37a932bfaca2698ba8026e7d35e0c0bb", null ],
    [ "ShipUp", "interface_space_game_logic_1_1_i_game_logic.html#a73746495b3cb63d069698c5eb0a0fc46", null ],
    [ "UsePowerUp", "interface_space_game_logic_1_1_i_game_logic.html#a0e9c00acc9cc0102c4ded91b176e9035", null ]
];