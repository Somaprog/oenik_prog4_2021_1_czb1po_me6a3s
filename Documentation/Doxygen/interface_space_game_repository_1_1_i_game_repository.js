var interface_space_game_repository_1_1_i_game_repository =
[
    [ "DeleteGame", "interface_space_game_repository_1_1_i_game_repository.html#af984aa2a7b5d97ba2d9cd09eae895abb", null ],
    [ "HighScoreSave", "interface_space_game_repository_1_1_i_game_repository.html#a85cfdf7fb45df813ae3064fd6200fa1c", null ],
    [ "LoadGame", "interface_space_game_repository_1_1_i_game_repository.html#ae9ae6b459953fcb9a32ca7d56615a45a", null ],
    [ "LoadHighScore", "interface_space_game_repository_1_1_i_game_repository.html#a0ca0199ead6b56e18f5a5195650fc772", null ],
    [ "SaveGame", "interface_space_game_repository_1_1_i_game_repository.html#af372ddf796b33c8dc58ed47a1e60069c", null ]
];