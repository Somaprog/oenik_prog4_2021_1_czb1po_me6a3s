var class_space_game_model_1_1_game_info =
[
    [ "GameInfo", "class_space_game_model_1_1_game_info.html#a3e4af8b197ba310c90edc422101b9664", null ],
    [ "GetHashCode", "class_space_game_model_1_1_game_info.html#af5964c959ed9e931aa25ef896f8475fd", null ],
    [ "Coin", "class_space_game_model_1_1_game_info.html#a748ba3d01c0eeb3adb3359a57360cd43", null ],
    [ "CurrLvl", "class_space_game_model_1_1_game_info.html#a154582e80c51d89c589e1775bd4fca78", null ],
    [ "CurrPUp", "class_space_game_model_1_1_game_info.html#a6a5a570e5054b079471d72a25c1c0b23", null ],
    [ "Hp", "class_space_game_model_1_1_game_info.html#a062d0f8fbf37680279c418d6a4c0a6a8", null ],
    [ "Score", "class_space_game_model_1_1_game_info.html#a633eaf9891b8736bae41259dfd1e744b", null ],
    [ "ShipLvl", "class_space_game_model_1_1_game_info.html#acf2d6f945d807b32c77a5479244c544f", null ],
    [ "ShipType", "class_space_game_model_1_1_game_info.html#a63e219b1ba77c270e9814628ab7ee0e3", null ]
];