﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("", "CA1014", Justification ="<NikGitStats>", Scope = "module")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<NikGitStats>", Scope = "namespaceanddescendants", Target = "~N:SpaceGameRenderer")]
[assembly: SuppressMessage("Style", "IDE0044:Add readonly modifier", Justification = "<NikGitStats>", Scope = "namespaceanddescendants", Target = "~N:SpaceGameRenderer")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<NikGitStats>", Scope = "namespaceanddescendants", Target = "~N:SpaceGameRenderer")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "<NikGitStats>", Scope = "namespaceanddescendants", Target = "~N:SpaceGameRenderer")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "<NikGitStats>", Scope = "namespaceanddescendants", Target = "~N:SpaceGameRenderer")]
