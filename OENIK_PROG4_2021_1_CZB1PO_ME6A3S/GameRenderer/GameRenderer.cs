﻿// <copyright file="GameRenderer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SpaceGameRenderer
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using SpaceGameModel;

    /// <summary>
    /// GameRenderer class.
    /// </summary>
    public class GameRenderer
    {
        private Pen black = new (Brushes.Black, 2);
        private Pen red = new (Brushes.Blue, 2);
        private Rect background;
        private GameModel model;

        private List<FormattedText> optionString;

        private FormattedText title;

        private Typeface font = new ("Arial");
        private Typeface fontRoman = new ("Arial");
        private Typeface fontCoin = new ("Arial");
        private Point coinNumLocation = new (10, 380);
        private Point textLocation = new (750, 10);
        private FormattedText formattedText;
        private FormattedText formattedTextCoinNum;
        private FormattedText formattedTextGameOver;
        private FormattedText formattedTextGameOverScore;
        private FormattedText backText;
        private FormattedText resumeText;
        private int oldHighScore = -1;
        private int oldCoinNum = -1;
        private int oldHighScoreGameOver = -1;
        private int oldPUpId = -1;
        private bool volt = true;

        private Dictionary<string, Brush> myBrushes = new ();

        /// <summary>
        /// Initializes a new instance of the <see cref="GameRenderer"/> class.
        /// </summary>
        /// <param name="model">GameModel model.</param>
        public GameRenderer(GameModel model)
        {
            this.model = model;
            this.background = new Rect(0, 0, model.GameWidth, model.GameHeight);
            this.OptionList = new List<Rect>();
            this.PauseList = new List<Rect>();
            this.optionString = new List<FormattedText>();
        }

        /// <summary>
        /// Gets or sets the menu optiosn.
        /// </summary>
        public List<Rect> OptionList { get; set; }

        /// <summary>
        /// Gets or sets the pause menu options.
        /// </summary>
        public List<Rect> PauseList { get; set; }

        /// <summary>
        /// Renders the game elements.
        /// </summary>
        /// <param name="ctx">Drawing context.</param>
        public void BuildGameDisplay(DrawingContext ctx)
        {
            this.OptionList.Clear();
            this.optionString.Clear();
            if (ctx is null)
            {
                throw new ArgumentNullException(nameof(ctx));
            }

            this.DrawBackGround(ctx);
            this.DrawShip(ctx);
            this.DrawCoins(ctx);
            this.DrawPowerUps(ctx);
            this.DrawAsteroids(ctx);
            this.DrawHighScore(ctx);
            this.DrawHealth(ctx);
            this.DrawCoinNumber(ctx);
            this.DrawActivePUp(ctx);
        }

        /// <summary>
        /// Builds Display.
        /// </summary>
        /// <param name="ctx">ctx.</param>
        /// <param name="coin">coin.</param>
        /// <param name="currLvl">lvl.</param>
        /// <param name="currPUp">pup.</param>
        /// <param name="score">score.</param>
        /// <param name="shipLvl">h.</param>
        /// <param name="shipType">type.</param>
        public void BuildGameDisplay(DrawingContext ctx, int coin, int currLvl, int currPUp, int score, int shipLvl, int shipType)
        {
            this.optionString.Clear();
            this.OptionList.Clear();
            if (ctx is null)
            {
                throw new ArgumentNullException(nameof(ctx));
            }

            this.model.CurrLvl = currLvl;
            this.model.CoinInventory = coin;
            this.model.CurrPup = currPUp;
            this.model.Score = score;
            this.model.ShipLvl = shipLvl;
            this.model.ShipType = shipType;
            this.DrawBackGround(ctx);
            this.DrawShip(ctx);
            this.DrawCoins(ctx);
            this.DrawPowerUps(ctx);
            this.DrawAsteroids(ctx);
            this.DrawHighScore(ctx);
            this.DrawHealth(ctx);
            this.DrawCoinNumber(ctx);
            this.DrawActivePUp(ctx);
            this.DrawBullet(ctx);
        }

        /// <summary>
        /// Renders the game elements when the game is over.
        /// </summary>
        /// <param name="ctx">Drawing context.</param>
        public void DrawGameOver(DrawingContext ctx)
        {
            this.OptionList.Clear();
            this.PauseList.Clear();
            if (ctx is null)
            {
                throw new ArgumentNullException(nameof(ctx));
            }

            if (this.volt)
            {
                this.volt = false;
                this.oldHighScoreGameOver = this.model.Score;
            }

            this.formattedTextGameOver = new FormattedText("You Died", System.Globalization.CultureInfo.CurrentCulture, FlowDirection.LeftToRight, this.fontRoman, 45, Brushes.DarkRed, 1);
            this.formattedTextGameOverScore = new FormattedText("Score:" + this.oldHighScoreGameOver, System.Globalization.CultureInfo.CurrentCulture, FlowDirection.LeftToRight, this.fontRoman, 30, Brushes.Orange, 1);
            this.DrawEndGameBackGround(ctx);
            ctx.DrawText(this.formattedTextGameOver, new Point(this.model.GameWidth / 2, this.model.GameHeight / 2));
            ctx.DrawText(this.formattedTextGameOverScore, new Point(this.model.GameWidth / 2, (this.model.GameHeight / 2) + 50));
        }

        /// <summary>
        /// Draws the Main Menu.
        /// </summary>
        /// <param name="ctx">Drawing context.</param>
        public void DrawMainMenu(DrawingContext ctx)
        {
            this.OptionList.Clear();
            this.optionString.Clear();
            if (ctx is null)
            {
                throw new ArgumentNullException(nameof(ctx));
            }

            this.DrawBackGround(ctx);
            this.DrawEndGameBackGround(ctx);

            this.OptionList.Add(new Rect((this.model.GameWidth / 2) - 80, this.model.GameHeight - 30, 140, 40));
            this.OptionList.Add(new Rect((this.model.GameWidth / 2) - 80, this.model.GameHeight - 100, 140, 40));
            this.OptionList.Add(new Rect((this.model.GameWidth / 2) - 80, this.model.GameHeight - 170, 160, 40));
            this.OptionList.Add(new Rect((this.model.GameWidth / 2) - 80, this.model.GameHeight - 240, 160, 40));
            this.OptionList.Add(new Rect((this.model.GameWidth / 2) - 80, this.model.GameHeight - 310, 160, 40));

            foreach (var item in this.OptionList)
            {
                ctx.DrawRectangle(null, null, item);
            }

            this.optionString.Clear();
            this.optionString.Add(new FormattedText("Ship Skins", CultureInfo.CurrentCulture, FlowDirection.LeftToRight, this.fontRoman, 30, Brushes.White, 1));
            this.optionString.Add(new FormattedText("High Score", CultureInfo.CurrentCulture, FlowDirection.LeftToRight, this.fontRoman, 30, Brushes.White, 1));
            this.optionString.Add(new FormattedText("Load Game", CultureInfo.CurrentCulture, FlowDirection.LeftToRight, this.fontRoman, 30, Brushes.White, 1));
            this.optionString.Add(new FormattedText("New Game", CultureInfo.CurrentCulture, FlowDirection.LeftToRight, this.fontRoman, 30, Brushes.White, 1));

            for (int i = 1; i < this.optionString.Count + 1; i++)
            {
                ctx.DrawText(this.optionString[i - 1], new Point(this.OptionList[i].X, this.OptionList[i].Y));
            }

            this.title = new FormattedText("Space Maze", CultureInfo.CurrentCulture, FlowDirection.LeftToRight, this.fontRoman, 50, Brushes.White, 1);
            ctx.DrawText(this.title, new Point((this.model.GameWidth / 2) - 150, this.model.GameHeight - 400));
        }

        /// <summary>
        /// Draws the HighScore menu.
        /// </summary>
        /// <param name="ctx">Drawing context.</param>
        public void DrawHSMenu(DrawingContext ctx)
        {
            this.OptionList.Clear();
            this.optionString.Clear();

            if (ctx is null)
            {
                throw new ArgumentNullException(nameof(ctx));
            }

            this.DrawEndGameBackGround(ctx);
            this.title = new FormattedText("High Scores", CultureInfo.CurrentCulture, FlowDirection.LeftToRight, this.fontRoman, 50, Brushes.White, 1);
            ctx.DrawText(this.title, new Point((this.model.GameWidth / 2) - 150, this.model.GameHeight - 400));
            for (int i = 0; i < this.model.HighScores.Count; i++)
            {
                FormattedText formattedScore = new (this.model.HighScores[i].Name + "\t\t" + this.model.HighScores[i].Score, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, this.fontRoman, 15, Brushes.PaleGoldenrod, 1);
                ctx.DrawText(formattedScore, new Point((this.model.GameWidth / 2) - 100, this.model.GameHeight - (320 - (i * 30))));
            }

            this.backText = new FormattedText("Back", CultureInfo.CurrentCulture, FlowDirection.LeftToRight, this.fontRoman, 20, Brushes.White, 1);

            this.OptionList.Add(new Rect((this.model.GameWidth / 2) - 35, this.model.GameHeight - 40, 40, 30));
            foreach (var item in this.OptionList)
            {
                ctx.DrawRectangle(null, null, item);
            }

            ctx.DrawText(this.backText, new Point((this.model.GameWidth / 2) - 35, this.model.GameHeight - 40));
        }

        /// <summary>
        /// Draw skin.
        /// </summary>
        /// <param name="ctx">ctx.</param>
        public void DrawSkinShop(DrawingContext ctx)
        {
            this.OptionList.Clear();
            this.optionString.Clear();

            if (ctx is null)
            {
                throw new ArgumentNullException(nameof(ctx));
            }

            this.DrawEndGameBackGround(ctx);
            this.title = new FormattedText("Skins Shop", CultureInfo.CurrentCulture, FlowDirection.LeftToRight, this.fontRoman, 50, Brushes.White, 1);
            ctx.DrawText(this.title, new Point((this.model.GameWidth / 2) - 150, this.model.GameHeight - 400));

            this.backText = new FormattedText("Back", System.Globalization.CultureInfo.CurrentCulture, FlowDirection.LeftToRight, this.fontRoman, 20, Brushes.White, 1);

            this.OptionList.Add(new Rect((this.model.GameWidth / 2) - 35, this.model.GameHeight - 40, 40, 30));
            foreach (var item in this.OptionList)
            {
                ctx.DrawRectangle(null, null, item);
            }

            ctx.DrawText(this.backText, new Point((this.model.GameWidth / 2) - 35, this.model.GameHeight - 40));
        }

        /// <summary>
        /// Draws Pause menu.
        /// </summary>
        /// <param name="ctx">Drawing context.</param>
        public void DrawPauseMenu(DrawingContext ctx)
        {
            this.OptionList.Clear();
            this.optionString.Clear();
            if (ctx is null)
            {
                throw new ArgumentNullException(nameof(ctx));
            }

            this.BuildGameDisplay(ctx);
            this.title = new FormattedText("Pause", CultureInfo.CurrentCulture, FlowDirection.LeftToRight, this.fontRoman, 50, Brushes.White, 1);
            ctx.DrawText(this.title, new Point((this.model.GameWidth / 2) - 100, this.model.GameHeight - 400));

            this.backText = new FormattedText("Exit", CultureInfo.CurrentCulture, FlowDirection.LeftToRight, this.fontRoman, 20, Brushes.White, 1);
            this.resumeText = new FormattedText("Resume", CultureInfo.CurrentCulture, FlowDirection.LeftToRight, this.fontRoman, 20, Brushes.White, 1);

            this.PauseList.Add(new Rect((this.model.GameWidth / 2) - 35, this.model.GameHeight - 40, 70, 30));
            this.PauseList.Add(new Rect((this.model.GameWidth / 2) - 35, this.model.GameHeight - 100, 70, 30));
            foreach (var item in this.PauseList)
            {
                ctx.DrawRectangle(null, null, item);
            }

            ctx.DrawText(this.backText, this.PauseList[0].Location);
            ctx.DrawText(this.resumeText, this.PauseList[1].Location);
        }

        /// <summary>
        /// Draws PUp.
        /// </summary>
        /// <param name="ctx">Drawing context.</param>
        public void DrawPUp(DrawingContext ctx)
        {
            if (ctx is null)
            {
                throw new ArgumentNullException(nameof(ctx));
            }

            this.BuildGameDisplay(ctx);
            Rect r = new (this.model.Ship.CX - 28, this.model.Ship.CY - 410, 40, 400);
            ctx.DrawRectangle(Brushes.CornflowerBlue, null, r);
        }

        /// <summary>
        /// Draws PUp.
        /// </summary>
        /// <param name="ctx">Drawing context.</param>
        public void DrawBullet(DrawingContext ctx)
        {
            ctx.DrawGeometry(this.GetBrush("bullet.png"), null, this.model.Bullet.RealArea);
        }

        private void DrawHealth(DrawingContext ctx)
        {
            foreach (var health in this.model.Hp)
            {
                ctx.DrawGeometry(this.GetBrush("health.png"), this.black, health.RealArea);
            }
        }

        private void DrawHighScore(DrawingContext ctx)
        {
            if (this.oldHighScore != this.model.Score)
            {
                this.formattedText = new FormattedText(this.model.Score.ToString(), System.Globalization.CultureInfo.CurrentCulture, FlowDirection.LeftToRight, this.font, 16, Brushes.White, 1);
            }

            ctx.DrawText(this.formattedText, this.textLocation);
        }

        private void DrawCoinNumber(DrawingContext ctx)
        {
            if (this.oldCoinNum != this.model.CoinInventory)
            {
                this.formattedTextCoinNum = new FormattedText(this.model.CoinInventory.ToString(), System.Globalization.CultureInfo.CurrentCulture, FlowDirection.LeftToRight, this.fontCoin, 16, Brushes.White, 1);
            }

            ctx.DrawText(this.formattedTextCoinNum, this.coinNumLocation);
        }

        private void DrawActivePUp(DrawingContext ctx)
        {
            Rect r = new (80, 350, 32, 32);
            EllipseGeometry pup2 = new (r);
            if (this.oldPUpId != this.model.CurrPup)
            {
                switch (this.model.CurrPup)
                {
                    case 0:
                        ctx.DrawGeometry(this.GetBrush("pup0.png"), null, pup2);
                        break;
                    case 1:
                        ctx.DrawGeometry(this.GetBrush("pup1.png"), null, pup2);
                        break;
                    case 2:
                        ctx.DrawGeometry(this.GetBrush("pup2.png"), null, pup2);
                        break;
                    case 3:
                        ctx.DrawGeometry(this.GetBrush("pup3.png"), null, pup2);
                        break;
                    case 4:
                        ctx.DrawGeometry(this.GetBrush("health.png"), null, pup2);
                        break;
                }
            }
        }

        private void DrawBackGround(DrawingContext ctx)
        {
            ctx.DrawRectangle(this.GetBrush($"background{this.model.CurrLvl}.jpg"), this.black, this.background);
        }

        private void DrawEndGameBackGround(DrawingContext ctx)
        {
            ctx.DrawRectangle(Brushes.Black, this.black, this.background);
        }

        private void DrawShip(DrawingContext ctx)
        {
            if (this.model.CurrPup == 3)
            {
                ctx.DrawGeometry(this.GetBrush("ship1.png"), this.red, this.model.Ship.RealArea);
            }
            else
            {
                ctx.DrawGeometry(this.GetBrush("ship1.png"), null, this.model.Ship.RealArea);
            }
        }

        private void DrawAsteroids(DrawingContext ctx)
        {
            foreach (var asteroid in this.model.Asteroids)
            {
                ctx.DrawGeometry(this.GetBrush("asteroid.png"), null, asteroid.RealArea);
            }
        }

        private void DrawCoins(DrawingContext ctx)
        {
            foreach (var coin in this.model.Coin)
            {
                ctx.DrawGeometry(this.GetBrush("coin.png"), null, coin.RealArea);
            }
        }

        private void DrawPowerUps(DrawingContext ctx)
        {
            foreach (var pup in this.model.PowerUps)
            {
                switch ((pup as PowerUpItem).ID)
                {
                    case 0:
                        ctx.DrawGeometry(this.GetBrush("pup0.png"), null, pup.RealArea); break;
                    case 1:
                        ctx.DrawGeometry(this.GetBrush("pup1.png"), null, pup.RealArea); break;
                    case 2:
                        ctx.DrawGeometry(this.GetBrush("pup2.png"), null, pup.RealArea); break;
                    case 3:
                        ctx.DrawGeometry(this.GetBrush("pup3.png"), null, pup.RealArea); break;
                    case 4:
                        ctx.DrawGeometry(this.GetBrush("health.png"), null, pup.RealArea); break;
                }
            }
        }

        private Brush GetBrush(string fname)
        {
            var vmi = Environment.CurrentDirectory;
            string newPath = Path.GetFullPath(Path.Combine(vmi, @"..\..\..\..\..\"));
            string filePath = Path.Combine(newPath, "models", fname);
            if (!this.myBrushes.ContainsKey(fname))
            {
                ImageBrush ib = new (new BitmapImage(new Uri(filePath)));
                this.myBrushes[fname] = ib;
            }

            return this.myBrushes[fname];
        }
    }
}
