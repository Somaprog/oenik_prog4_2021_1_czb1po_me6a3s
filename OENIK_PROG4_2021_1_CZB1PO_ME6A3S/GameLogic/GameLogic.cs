﻿// <copyright file="GameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SpaceGameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using SpaceGameModel;
    using SpaceGameRepository;

    /// <summary>
    /// Main logic class.
    /// </summary>
    public class GameLogic : IGameLogic
    {
        /// <summary>
        /// model entity.
        /// </summary>
        private IGameModel model;

        /// <summary>
        /// model entity.
        /// </summary>
        private IGameRepository repo = new GameRepository();

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class.
        /// </summary>
        /// <param name="model">model entity.</param>
        public GameLogic(IGameModel model)
        {
            this.model = model;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class.ű
        /// Default constructor for usual use, it creates instances
        /// of the mocked repositories.
        /// </summary>
        /// <param name="model">model entity.</param>
        /// <param name="repo">repo entity.</param>
        public GameLogic(IGameModel model, IGameRepository repo)
        {
            this.model = model;
            this.repo = repo;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class when new game is choosen.
        /// </summary>
        /// <param name="w">window width.</param>
        /// <param name="h">window height.</param>
        public GameLogic(double w, double h)
        {
            this.model = new GameModel(w, h);
        }

        /// <inheritdoc/>
        public void AsteroidMove(double width, double height)
        {
            foreach (var asteroid in this.model.Asteroids)
            {
                asteroid.CY += 5;

                Random r = new ();

                if (asteroid.CY > 1600)
                {
                    asteroid.CX = r.Next(0, 350);
                    asteroid.CY = r.Next(-1200, -10);
                }
            }
        }

        /// <inheritdoc/>
        public void BulletMove(double width, double height)
        {
            (this.model.Bullet as BulletItem).CY -= 5;
        }

        /// <inheritdoc/>
        public void AutoSave()
        {
            this.repo.SaveGame(new GameInfo(
                this.model.Score,
                this.model.CoinInventory,
                this.model.CurrLvl,
                this.model.CurrPup,
                this.model.ShipType,
                this.model.ShipLvl,
                this.model.Hp.Count));
        }

        /// <inheritdoc/>
        public void ChangeLvl()
        {
            this.model.CurrLvl++;
        }

        /// <inheritdoc/>
        public void ChooseShip(int type)
        {
            this.model.ShipType = type;
        }

        /// <inheritdoc/>
        public void CoinMove(double width, double height)
        {
            foreach (var coin in this.model.Coin)
            {
                coin.CY += 5;

                Random r = new ();

                if (coin.CY > 1600)
                {
                    coin.CX = r.Next(0, 350);
                    coin.CY = r.Next(-1200, -10);
                }
            }
        }

        /// <inheritdoc/>
        public void CoinUp(int x)
        {
            this.model.CoinInventory += x;
        }

        /// <inheritdoc/>
        public void Collided()
        {
            if (this.model.CurrPup == 3)
            {
                this.model.CurrPup = -1;
            }
            else
            {
                if (this.model.Hp.Count == 3)
                {
                    this.model.Hp.RemoveAll(x => x.CX == 340);
                }
                else if (this.model.Hp.Count == 2)
                {
                    this.model.Hp.RemoveAll(x => x.CX == 360);
                }
                else if (this.model.Hp.Count == 1)
                {
                    this.model.Hp.RemoveAll(x => x.CX == 380);
                }

                if (this.model.Hp.Count == 0)
                {
                    this.GameOver();
                }
            }
        }

        /// <inheritdoc/>
        public void DeleteSave()
        {
            this.repo.DeleteGame();
        }

        /// <inheritdoc/>
        public void GameOver()
        {
            this.repo.HighScoreSave("faszom", this.model.Score, this.model.HighScores);
            this.DeleteSave();
        }

        /// <inheritdoc/>
        public void HpUp()
        {
            if (this.model.Hp.Count == 2)
            {
                this.model.Hp.Add(new HealthItem(340, 190));
            }
            else if (this.model.Hp.Count == 1)
            {
                this.model.Hp.Add(new HealthItem(360, 190));
            }
        }

        /// <inheritdoc/>
        public void CheckCollide()
        {
            Random r = new ();
            foreach (IGameItem item in this.model.Asteroids)
            {
                if (this.model.Ship.CX - item.CX > -700 && this.model.Ship.CX - item.CX < 700)
                {
                    if (this.model.Ship.CY - item.CY > -700 && this.model.Ship.CY - item.CY < 700)
                    {
                        if (item.IsCollision(this.model.Ship))
                        {
                            item.CX = r.Next(0, 350);
                            item.CY = r.Next(-1200, -10);
                            this.Collided();
                        }
                    }
                }
            }

            foreach (IGameItem item in this.model.Coin)
            {
                if (this.model.Ship.CX - item.CX > -500 && this.model.Ship.CX - item.CX < 500)
                {
                    if (this.model.Ship.CY - item.CY > -500 && this.model.Ship.CY - item.CY < 500)
                    {
                        if (item.IsCollision(this.model.Ship))
                        {
                            item.CX = r.Next(0, 350);
                            item.CY = r.Next(-1200, -10);
                            this.CoinUp(r.Next(10, 150));
                        }
                    }
                }
            }

            if (this.model.PowerUps.Count > 0)
            {
                foreach (PowerUpItem item in this.model.PowerUps)
                {
                    if (this.model.Ship.CX - item.CX > -500 && this.model.Ship.CX - item.CX < 500)
                    {
                        if (this.model.Ship.CY - item.CY > -500 && this.model.Ship.CY - item.CY < 500)
                        {
                            if (item.IsCollision(this.model.Ship))
                            {
                                item.CX = r.Next(0, 350);
                                item.CY = r.Next(-12000, -800);
                                if (item.ID != 4)
                                {
                                    this.PickPUp(item.ID);
                                }
                                else
                                {
                                    this.HpUp();
                                }

                                item.ID = r.Next(0, 5);
                            }
                        }
                    }
                }
            }
        }

        /// <inheritdoc/>
        public void LoadHighscores()
        {
            this.model.HighScores = this.repo.LoadHighScore();
        }

        /// <inheritdoc/>
        public GameInfo LoadSave()
        {
            return this.repo.LoadGame();
        }

        /// <inheritdoc/>
        public void MoveShip(double x, double y)
        {
            this.model.Ship.CX = x;
            this.model.Ship.CY = y;
        }

        /// <inheritdoc/>
        public void PickPUp(int id)
        {
            this.model.CurrPup = id;
        }

        /// <inheritdoc/>
        public void PUpMove(double width, double height)
        {
            foreach (var pup in this.model.PowerUps)
            {
                pup.CY += 5;

                Random r = new ();

                if (pup.CY > 1600)
                {
                    pup.CX = r.Next(0, 350);
                    pup.CY = r.Next(-1200, -10);
                }
            }
        }

        /// <inheritdoc/>
        public void ScoreUp(int x)
        {
            this.model.Score += x;
        }

        /// <inheritdoc/>
        public void ShipUp()
        {
            this.model.ShipLvl++;
        }

        /// <inheritdoc/>
        public void UsePowerUp()
        {
            this.model.CurrPup = -1;
        }
    }
}
