﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("", "CA1014", Justification ="<NikGitStats>", Scope = "module")]
[assembly: SuppressMessage("Style", "IDE0044:Add readonly modifier", Justification = "<NikGitStats>", Scope = "namespaceanddescendants", Target = "~N:SpaceGameLogic")]
[assembly: SuppressMessage("Security", "CA5394:Do not use insecure randomness", Justification = "<NikGitStats>", Scope = "namespaceanddescendants", Target = "~N:SpaceGameLogic")]