﻿// <copyright file="IGameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SpaceGameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using SpaceGameModel;

    /// <summary>
    /// Logic interface.
    /// </summary>
    public interface IGameLogic
    {
        /// <summary>
        /// Called by time, sets the ship to the current location of the mouse.
        /// </summary>
        /// <param name="x">x coordinate.</param>
        /// <param name="y">y coordinate.</param>
        void MoveShip(double x, double y);

        /// <summary>
        /// Using powerup.
        /// </summary>
        void UsePowerUp();

        /// <summary>
        /// Score.
        /// </summary>
        /// <param name="x">multiplier.</param>
        void ScoreUp(int x);

        /// <summary>
        /// Coin.
        /// </summary>
        /// <param name="x">multiplier.</param>
        void CoinUp(int x);

        /// <summary>
        /// Ship level up.
        /// </summary>
        void ShipUp();

        /// <summary>
        /// HP up.
        /// </summary>
        void HpUp();

        /// <summary>
        /// Fired when collided with asteroid.
        /// </summary>
        void Collided();

        /// <summary>
        /// Main ship type.
        /// </summary>
        /// <param name="type">main category.</param>
        void ChooseShip(int type);

        /// <summary>
        /// Changes level when fired.
        /// </summary>
        void ChangeLvl();

        /// <summary>
        /// Saving.
        /// </summary>
        void AutoSave();

        /// <summary>
        /// Deleting autosave.
        /// </summary>
        void DeleteSave();

        /// <summary>
        /// Check if ship collided with anything.
        /// </summary>
        public void CheckCollide();

        /// <summary>
        /// Gameover function.
        /// </summary>
        void GameOver();

        /// <summary>
        /// Moves bullet.
        /// </summary>
        /// <param name="width">width.</param>
        /// <param name="height">height.</param>
        public void BulletMove(double width, double height);

        /// <summary>
        /// Picking powerUp up.
        /// </summary>
        /// <param name="id">main category.</param>
        void PickPUp(int id);

        /// <summary>
        /// Loads highscores to model.
        /// </summary>
        public void LoadHighscores();

        /// <summary>
        /// Return the GameInfo.
        /// </summary>
        /// <returns>GameInfo.</returns>
        public GameInfo LoadSave();

        /// <summary>
        /// Moves the asteroid object.
        /// </summary>
        /// <param name="width">window width.</param>
        /// <param name="height">window height.</param>
        void AsteroidMove(double width, double height);

        /// <summary>
        /// Moves the Coin object.
        /// </summary>
        /// <param name="width">window width.</param>
        /// <param name="height">window height.</param>
        void CoinMove(double width, double height);

        /// <summary>
        /// Moves the PUp object.
        /// </summary>
        /// <param name="width">window width.</param>
        /// <param name="height">window height.</param>
        void PUpMove(double width, double height);
    }
}
