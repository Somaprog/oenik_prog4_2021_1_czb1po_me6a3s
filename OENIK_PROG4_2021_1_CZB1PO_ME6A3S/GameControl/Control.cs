﻿// <copyright file="Control.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SpaceGameControl
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Threading;
    using SpaceGameLogic;
    using SpaceGameModel;
    using SpaceGameRenderer;

    /// <summary>
    /// Game control class.
    /// </summary>
    public class Control : FrameworkElement
    {
        private static int menuID = 4;
        private static int counter;

        private GameModel model;
        private GameLogic logic;
        private GameRenderer renderer;
        private DispatcherTimer mainTimer;
        private Stopwatch timer;

        /// <summary>
        /// Initializes a new instance of the <see cref="Control"/> class.
        /// </summary>
        public Control()
        {
            this.Loaded += this.ScreenLoaded;
        }

        /// <inheritdoc/>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.renderer != null)
            {
                // this.renderer.BuildGameDisplay(drawingContext);
                switch (menuID)
                {
                    case 0: // skinshop
                        this.renderer.DrawSkinShop(drawingContext);
                        this.mainTimer.Stop();
                        this.timer.Stop();
                        break;
                    case 1: // highscore
                        this.renderer.DrawHSMenu(drawingContext);
                        this.mainTimer.Stop();
                        this.timer.Stop();
                        break;
                    case 3: // new game
                        this.renderer.BuildGameDisplay(drawingContext);
                        this.Cursor = Cursors.None;
                        this.mainTimer.Start();
                        if (!this.timer.IsRunning)
                        {
                            this.timer.Start();
                        }

                        break;
                    case 4: // main menu
                        this.renderer.DrawMainMenu(drawingContext);
                        this.mainTimer.Stop();
                        this.timer.Stop();
                        break;
                    case 5: // pause
                        this.renderer.DrawPauseMenu(drawingContext);
                        this.mainTimer.Stop();
                        this.Cursor = Cursors.Arrow;
                        this.timer.Stop();
                        break;
                    case 6: // poqer up draw
                        if (this.model.CurrPup == 0)
                        {
                            this.renderer.DrawPUp(drawingContext);
                            counter = Convert.ToInt32(this.timer.ElapsedMilliseconds);
                        }
                        else if (this.model.CurrPup == 1)
                        {
                            foreach (AsteroidItem item in this.model.Asteroids)
                            {
                                item.CY = 1500;
                            }

                            this.logic.UsePowerUp();
                        }
                        else if (this.model.CurrPup == 2)
                        {
                            this.model.Bullet.CX = this.model.Ship.CX;
                            this.model.Bullet.CY = this.model.Ship.CY;
                            this.logic.UsePowerUp();
                        }

                        break;
                    case 2: // loadgame case 2
                        GameInfo vmi = this.logic.LoadSave();

                        this.renderer.BuildGameDisplay(drawingContext, vmi.Coin, vmi.CurrLvl, vmi.CurrPUp, vmi.Score, vmi.ShipLvl, vmi.ShipType);
                        this.Cursor = Cursors.None;
                        this.mainTimer.Start();
                        if (!this.timer.IsRunning)
                        {
                            this.timer.Start();
                        }

                        break;
                }

                if (!this.model.Hp.Any())
                {
                    this.renderer.DrawGameOver(drawingContext);
                    this.Cursor = Cursors.Arrow;
                }
            }
        }

        private void ScreenLoaded(object sender, RoutedEventArgs e)
        {
            this.model = new GameModel(this.ActualWidth, this.ActualHeight);
            this.logic = new GameLogic(this.model);
            this.logic.LoadHighscores();
            this.renderer = new GameRenderer(this.model);

            Window win = Window.GetWindow(this);
            if (win != null)
            {
                win.KeyDown += this.ScreenKeyDown;
                win.MouseMove += this.ScreenMouseMove;
                win.MouseLeftButtonDown += this.MouseClick;
                win.PreviewTextInput += this.TextInput;
                this.timer = new Stopwatch();
                this.mainTimer = new DispatcherTimer
                {
                    Interval = TimeSpan.FromMilliseconds(20),
                };
                this.mainTimer.Tick += this.TimerTick;
                this.mainTimer.Start();
            }

            this.InvalidateVisual();
        }

        private void ScreenKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Escape: menuID = 5; break;
                case Key.Space: menuID = 3; break;
                case Key.LeftShift: menuID = 6; break;
            }

            // this.InvalidateVisual();
        }

        private void ScreenMouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            Point position = e.GetPosition(this);
            this.logic.MoveShip(position.X, position.Y);

            // this.InvalidateVisual();
        }

        private void TimerTick(object sender, EventArgs e)
        {
            this.logic.AsteroidMove(this.ActualWidth, this.ActualHeight);
            this.logic.BulletMove(this.ActualWidth, this.ActualHeight);
            this.logic.CoinMove(this.ActualWidth, this.ActualHeight);
            this.logic.PUpMove(this.ActualWidth, this.ActualHeight);
            this.logic.ScoreUp(Convert.ToInt32(this.timer.ElapsedMilliseconds) / 5000);
            this.logic.CheckCollide();
            if (this.model.Score % 550 == 0)
            {
                this.logic.AutoSave();
            }

            if (this.model.Score % 1500 == 0)
            {
                this.logic.ChangeLvl();
            }

            if (menuID == 6)
            {
                if ((this.timer.ElapsedMilliseconds - counter) > 3000)
                {
                    menuID = 3;
                }
            }

            this.InvalidateVisual();
        }

        private void MouseClick(object sender, MouseButtonEventArgs e)
        {
            Point pos = e.GetPosition(this);

            if (e.LeftButton == MouseButtonState.Pressed)
            {
                for (int i = 0; i < this.renderer.OptionList.Count; i++)
                {
                    if (this.renderer.OptionList[i].Contains(pos))
                    {
                        switch (i)
                        {
                            case 1: menuID = 0; break; // option list 1. elem skinshop
                            case 2: menuID = 1; break; // option list 2. highscore
                            case 0: menuID = 4; break;  // option list 0. back
                            case 4: menuID = 3; break;  // option list 4. elem new game
                            case 3: menuID = 2;  // option list 3. elem load game
                                break;
                        }
                    }
                }

                for (int i = 0; i < this.renderer.PauseList.Count; i++)
                {
                    if (this.renderer.PauseList[i].Contains(pos))
                    {
                        switch (i)
                        {
                            case 0: menuID = 4; break;
                            case 1: menuID = 3; break;
                        }
                    }
                }
            }

            this.InvalidateVisual();
        }

        private new void TextInput(object sender, TextCompositionEventArgs e)
        {
            // string input = e.Text;

            // TODO: név bekérés
        }
    }
}