﻿// <copyright file="GameModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SpaceGameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Model class.
    /// </summary>
    public class GameModel : IGameModel
    {
        private const int AsteroidNum = 70;

        private Random r = new ();

        /// <summary>
        /// Initializes a new instance of the <see cref="GameModel"/> class.
        /// </summary>
        /// <param name="w">Asteroid width.</param>
        /// <param name="h">Asteroid height.</param>
        public GameModel(double w, double h)
        {
            this.GameWidth = w;
            this.GameHeight = h;
            this.CoinInventory = 0;
            this.Score = 1;
            this.CurrPup = -1;

            this.Ship = new ShipItem((w / 4) - 220, (h / 4) - 115);
            this.Asteroids = new List<IGameItem>();
            this.Hp = new List<IGameItem>();
            this.Coin = new List<IGameItem>();
            this.PowerUps = new List<IGameItem>();
            this.Bullet = new BulletItem();

            this.GenerateAsteroids();
            this.GenerateCoin();
            this.GeneratePowerUp();
            this.GenerateHealth();
        }

        /// <summary>
        /// Gets or sets current score in the game.
        /// </summary>
        public int Score { get; set; }

        /// <summary>
        /// Gets or sets current coin in the game.
        /// </summary>
        public List<IGameItem> Coin { get; set; }

        /// <summary>
        /// Gets or sets current Level in the game.
        /// </summary>
        public int CurrLvl { get; set; }

        /// <summary>
        /// Gets or sets current powerup in the game.
        /// </summary>
        public int CurrPup { get; set; }

        /// <summary>
        /// Gets or sets current ship type in the game.
        /// </summary>
        public int ShipType { get; set; }

        /// <summary>
        /// Gets or sets current ship level in the game.
        /// </summary>
        public int ShipLvl { get; set; }

        /// <summary>
        /// Gets current Hp in the game.
        /// </summary>
        public List<IGameItem> Hp { get; }

        /// <summary>
        /// Gets or sets current ship entity in the game.
        /// </summary>
        public IGameItem Ship { get; set; }

        /// <summary>
        /// Gets or sets current bullet entity in the game.
        /// </summary>
        public IGameItem Bullet { get; set; }

        /// <summary>
        /// Gets or sets current asteroids in the game.
        /// </summary>
        public List<IGameItem> Asteroids { get; set; }

        /// <summary>
        /// Gets or sets higscores in the game.
        /// </summary>
        public List<HighScore> HighScores { get; set; }

        /// <summary>
        /// Gets or sets current powerups in the game.
        /// </summary>
        public List<IGameItem> PowerUps { get; set; }

        /// <summary>
        /// Gets or sets the width of the game window.
        /// </summary>
        public double GameWidth { get; set; }

        /// <summary>
        /// Gets or sets the height of the game window.
        /// </summary>
        public double GameHeight { get; set; }

        /// <inheritdoc/>
        public int CoinInventory { get; set; }

        private void GenerateAsteroids()
        {
            for (int i = 0; i < AsteroidNum; i++)
            {
                bool megvan = true;
                int x;
                int y;
                while (megvan)
                {
                    x = this.r.Next(0, 350);
                    y = this.r.Next(-1200, -10);
                    bool talalat = false;
                    AsteroidItem newA = new (x, y);
                    foreach (IGameItem item in this.Asteroids)
                    {
                        if (item.IsCollision(newA))
                        {
                            talalat = true;
                        }
                    }

                    if (!talalat)
                    {
                        megvan = false;
                        this.Asteroids.Add(newA);
                    }
                }
            }
        }

        private void GenerateCoin()
        {
            for (int i = 0; i < AsteroidNum / 5; i++)
            {
                bool megvan = true;
                int x;
                int y;
                while (megvan)
                {
                    x = this.r.Next(0, 350);
                    y = this.r.Next(-1200, -10);
                    bool talalat = false;
                    CoinItem newC = new (x, y);
                    foreach (IGameItem item in this.Coin)
                    {
                        if (item.IsCollision(newC))
                        {
                            talalat = true;
                        }
                    }

                    if (!talalat)
                    {
                        megvan = false;
                        this.Coin.Add(newC);
                    }
                }
            }
        }

        private void GeneratePowerUp()
        {
            for (int i = 0; i < AsteroidNum / 5; i++)
            {
                bool megvan = true;
                int x;
                int y;
                int id;
                while (megvan)
                {
                    x = this.r.Next(0, 350);
                    y = this.r.Next(-1200, -10);
                    id = this.r.Next(0, 6);
                    bool talalat = false;
                    PowerUpItem newP = new (id, x, y);
                    foreach (IGameItem item in this.PowerUps)
                    {
                        if (item.IsCollision(newP))
                        {
                            talalat = true;
                        }
                    }

                    if (!talalat)
                    {
                        megvan = false;
                        this.PowerUps.Add(newP);
                    }
                }
            }
        }

        private void GenerateHealth()
        {
            this.Hp.Add(new HealthItem(380, 190));
            this.Hp.Add(new HealthItem(360, 190));
            this.Hp.Add(new HealthItem(340, 190));
        }
    }
}
