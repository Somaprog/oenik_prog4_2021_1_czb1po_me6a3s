﻿// <copyright file="HealthItem.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SpaceGameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// HealthItem class.
    /// </summary>
    public class HealthItem : GameItem
    {
        private double width = 10;
        private double height = 10;

        /// <summary>
        /// Initializes a new instance of the <see cref="HealthItem"/> class.
        /// </summary>
        /// <param name="cx">X coordinate.</param>
        /// <param name="cy">Y coordinate.</param>
        /// <param name="active">Is the health item active.</param>
        public HealthItem(double cx, double cy, bool active = true)
        {
            this.CX = cx;
            this.CY = cy;

            this.IsActive = active;

            GeometryGroup g = new ();
            Rect r = new (this.CX, this.CY, this.width, this.height);
            g.Children.Add(new EllipseGeometry(r));
            this.Area = g;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the health item is active or not.
        /// </summary>
        public bool IsActive { get; set; }
    }
}
