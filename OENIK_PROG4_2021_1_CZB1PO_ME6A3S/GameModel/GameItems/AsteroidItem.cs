﻿// <copyright file="AsteroidItem.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SpaceGameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Shapes;

    /// <summary>
    /// AsteroidItem class.
    /// </summary>
    public class AsteroidItem : GameItem
    {
        private static Random rnd = new ();

        /// <summary>
        /// Asteroid width.
        /// </summary>
        private double width = 100;

        /// <summary>
        /// Asteroid height.
        /// </summary>
        private double height = 100;

        /// <summary>
        /// Initializes a new instance of the <see cref="AsteroidItem"/> class.
        /// </summary>
        /// <param name="cx">X coordinate.</param>
        /// <param name="cy">Y coordinate.</param>
        public AsteroidItem(double cx = 0, double cy = 0)
        {
            double random = rnd.NextDouble();
            this.width *= random;
            this.height *= random;
            this.CX = cx;
            this.CY = cy;

            GeometryGroup g = new ();
            Rect r = new (this.CX, this.CY, this.width, this.height);
            g.Children.Add(new EllipseGeometry(r));
            this.Area = g;
        }
    }
}
