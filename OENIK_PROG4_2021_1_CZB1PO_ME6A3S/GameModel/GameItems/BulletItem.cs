﻿// <copyright file="BulletItem.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SpaceGameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// for bullets.
    /// </summary>
    public class BulletItem : GameItem
    {
        /// <summary>
        /// Ship width.
        /// </summary>
        private double width = 35;

        /// <summary>
        /// Ship height.
        /// </summary>
        private double height = 50;

        /// <summary>
        /// Initializes a new instance of the <see cref="BulletItem"/> class.
        /// </summary>
        /// <param name="cx">x coordinate.</param>
        /// <param name="cy">y coordinate.</param>
        public BulletItem(double cx = 0, double cy = 0)
        {
            this.CX = cx;
            this.CY = cy;
            this.DX = 10;

            GeometryGroup g = new ();
            Rect r = new (this.CX, this.CY, this.width, this.height);
            g.Children.Add(new EllipseGeometry(r));
            this.Area = g;
        }

        /// <summary>
        /// Gets or sets the Y velocity.
        /// </summary>
        public double DY { get; set; }

        /// <summary>
        /// Gets or sets the X velocity.
        /// </summary>
        public double DX { get; set; }
    }
}
