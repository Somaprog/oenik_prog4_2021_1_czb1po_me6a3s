﻿// <copyright file="PowerUpItem.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SpaceGameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// PowerUp ingame item.
    /// </summary>
    public class PowerUpItem : GameItem
    {
        /// <summary>
        /// Gets or sets id of pUp.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Powerup width.
        /// </summary>
        private double width = 30;

        /// <summary>
        /// PowerUp height.
        /// </summary>
        private double height = 30;

        /// <summary>
        /// Initializes a new instance of the <see cref="PowerUpItem"/> class.
        /// </summary>
        /// <param name="cx">X coordinate.</param>
        /// <param name="cy">Y coordinate.</param>
        /// <param name="iD">pUp id.</param>
        public PowerUpItem(int iD, double cx = 0, double cy = 0)
        {
            this.CX = cx;
            this.CY = cy;
            this.ID = iD;

            GeometryGroup g = new GeometryGroup();
            Rect r = new Rect(this.CX, this.CY, this.width, this.height);
            g.Children.Add(new EllipseGeometry(r));
            this.Area = g;
        }
    }
}
