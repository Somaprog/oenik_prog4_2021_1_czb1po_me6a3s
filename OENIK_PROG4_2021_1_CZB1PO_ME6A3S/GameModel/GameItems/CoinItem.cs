﻿// <copyright file="CoinItem.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SpaceGameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// CoinItem class.
    /// </summary>
    internal class CoinItem : GameItem
    {
        private double width = 20;

        private double height = 20;

        /// <summary>
        /// Initializes a new instance of the <see cref="CoinItem"/> class.
        /// </summary>
        /// <param name="cx">X coordinate.</param>
        /// <param name="cy">Y coordinate.</param>
        public CoinItem(double cx = 0, double cy = 0)
        {
            this.CX = cx;
            this.CY = cy;

            GeometryGroup g = new ();
            Rect r = new (this.CX, this.CY, this.width, this.height);
            g.Children.Add(new EllipseGeometry(r));
            this.Area = g;
        }
    }
}
