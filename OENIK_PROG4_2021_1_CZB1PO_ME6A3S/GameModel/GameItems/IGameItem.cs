﻿// <copyright file="IGameItem.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SpaceGameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;

    /// <summary>
    /// Gameitem iface for factory method.
    /// </summary>
    public interface IGameItem
    {
        /// <summary>
        /// Gets or sets center position when rendering.
        /// </summary>
        public double CX { get; set; }

        /// <summary>
        /// Gets or sets center position when rendering.
        /// </summary>
        public double CY { get; set; }

        /// <summary>
        /// Gets or sets radian conversion.
        /// </summary>
        public double Rad { get; set; }

        /// <summary>
        /// Gets the real area of the item.
        /// </summary>
        public Geometry RealArea { get; }

        /// <summary>
        /// Determines whether two objects collided.
        /// </summary>
        /// <param name="other">IGameItem object.</param>
        /// <returns>true or false.</returns>
        public bool IsCollision(IGameItem other);
    }
}
