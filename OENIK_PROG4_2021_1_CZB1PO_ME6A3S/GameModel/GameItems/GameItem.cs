﻿// <copyright file="GameItem.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SpaceGameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;

    /// <summary>
    /// GameItem class.
    /// </summary>
    public abstract class GameItem : IGameItem
    {
        /// <inheritdoc/>
        public Geometry RealArea
        {
            get
            {
                TransformGroup tg = new ();
                tg.Children.Add(new TranslateTransform(this.CX, this.CY));
                tg.Children.Add(new RotateTransform(this.RotDegree, this.CX, this.CY));
                this.Area.Transform = tg;
                return this.Area.GetFlattenedPathGeometry();
            }
        }

        /// <inheritdoc/>
        public double Rad
        {
            get
            {
                return this.RotDegree * Math.PI / 180;
            }

            set
            {
                this.RotDegree = 180 * value / Math.PI;
            }
        }

        /// <inheritdoc/>
        public double CX { get; set; }

        /// <inheritdoc/>
        public double CY { get; set; }

        /// <summary>
        /// Gets or sets (0;0) centered.
        /// </summary>
        protected Geometry Area { get; set; }

        /// <summary>
        /// Gets or sets stores rotation in degrees.
        /// </summary>
        private double RotDegree { get; set; }

        /// <inheritdoc/>
        public bool IsCollision(IGameItem other)
        {
            if (other is null)
            {
                throw new ArgumentNullException(nameof(other));
            }

            return Geometry.Combine(this.RealArea, other.RealArea, GeometryCombineMode.Intersect, null).GetArea() > 0;
        }
    }
}
