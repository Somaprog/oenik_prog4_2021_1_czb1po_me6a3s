﻿// <copyright file="GameInfo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SpaceGameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// SaveGame entity.
    /// </summary>
    [DataContract]
    public class GameInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameInfo"/> class.
        /// </summary>
        /// <param name="score">current score.</param>
        /// <param name="coin">current coin.</param>
        /// <param name="currlvl">currrent level.</param>
        /// <param name="currpup">current powerup.</param>
        /// <param name="shiptype">current shiptype.</param>
        /// <param name="shiplvl">current shiplevel.</param>
        /// <param name="hp">current hp.</param>
        public GameInfo(int score, int coin, int currlvl, int currpup, int shiptype, int shiplvl, int hp)
        {
            this.Score = score;
            this.Coin = coin;
            this.CurrLvl = currlvl;
            this.CurrPUp = currpup;
            this.ShipType = shiptype;
            this.ShipLvl = shiplvl;
            this.Hp = hp;
        }

        /// <summary>
        /// Gets or sets current score in the game.
        /// </summary>
        [DataMember]
        public int Score { get; set; }

        /// <summary>
        /// Gets or sets current coin in the game.
        /// </summary>
        [DataMember]
        public int Coin { get; set; }

        /// <summary>
        /// Gets or sets current Level in the game.
        /// </summary>
        [DataMember]
        public int CurrLvl { get; set; }

        /// <summary>
        /// Gets or sets current powerup in the game.
        /// </summary>
        [DataMember]
        public int CurrPUp { get; set; }

        /// <summary>
        /// Gets or sets current ship type in the game.
        /// </summary>
        [DataMember]
        public int ShipType { get; set; }

        /// <summary>
        /// Gets or sets current ship level in the game.
        /// </summary>
        [DataMember]
        public int ShipLvl { get; set; }

        /// <summary>
        /// Gets or sets current Hp in the game.
        /// </summary>
        [DataMember]
        public int Hp { get; set; }

        /// <summary>
        /// Gethashcode override.
        /// </summary>
        /// <returns>Hashcode.</returns>
        public override int GetHashCode()
        {
            string result;
            if (this.CurrPUp == -1)
            {
                result = this.Score.ToString() + this.Coin.ToString() + this.CurrLvl.ToString() + 5.ToString() + this.ShipType.ToString() + this.ShipLvl.ToString() + this.Hp.ToString();
            }
            else
            {
                result = this.Score.ToString() + this.Coin.ToString() + this.CurrLvl.ToString() + this.CurrPUp.ToString() + this.ShipType.ToString() + this.ShipLvl.ToString() + this.Hp.ToString();
            }

            return int.Parse(result);
        }
    }
}
