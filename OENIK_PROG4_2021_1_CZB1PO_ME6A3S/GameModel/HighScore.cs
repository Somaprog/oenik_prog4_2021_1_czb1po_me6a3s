﻿// <copyright file="HighScore.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SpaceGameModel
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Highscore class.
    /// </summary>
    [DataContract]
    public class HighScore
    {
        /// <summary>
        /// Gets or sets name of player.
        /// </summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets score of player.
        /// </summary>
        [DataMember]
        public int Score { get; set; }
    }
}
