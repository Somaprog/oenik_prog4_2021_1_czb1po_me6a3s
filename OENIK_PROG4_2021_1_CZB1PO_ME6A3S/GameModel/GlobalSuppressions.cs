﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("", "CA1014", Justification ="<NikGitStats>", Scope = "module")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "<NikGitStats>", Scope = "namespaceanddescendants", Target = "~N:SpaceGameModel")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "<NikGitStats>", Scope = "namespaceanddescendants", Target = "~N:SpaceGameModel")]
[assembly: SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1201:Elements should appear in the correct order", Justification = "<NikGitStats>", Scope = "namespaceanddescendants", Target = "~N:SpaceGameModel")]
[assembly: SuppressMessage("Security", "CA5394:Do not use insecure randomness", Justification = "<NikGitStats>", Scope = "namespaceanddescendants", Target = "~N:SpaceGameModel")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<NikGitStats>", Scope = "namespaceanddescendants", Target = "~N:SpaceGameModel")]
[assembly: SuppressMessage("Style", "IDE0044:Add readonly modifier", Justification = "<NikGitStats>", Scope = "namespaceanddescendants", Target = "~N:SpaceGameModel")]
