﻿// <copyright file="IGameModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SpaceGameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Model interface.
    /// </summary>
    public interface IGameModel
    {
        /// <summary>
        /// Gets or sets the width of the game window.
        /// </summary>
        double GameWidth { get; set; }

        /// <summary>
        /// Gets or sets the height of the game window.
        /// </summary>
        double GameHeight { get; set; }

        /// <summary>
        /// Gets or sets current score in the game.
        /// </summary>
        int Score { get; set; }

        /// <summary>
        /// Gets or sets current coin in the game.
        /// </summary>
        List<IGameItem> Coin { get; set; }

        /// <summary>
        /// Gets or sets the current amount of coins in the possession of the player.
        /// </summary>
        int CoinInventory { get; set; }

        /// <summary>
        /// Gets or sets current Level in the game.
        /// </summary>
        int CurrLvl { get; set; }

        /// <summary>
        /// Gets or sets current powerup in the game.
        /// </summary>
        public int CurrPup { get; set; }

        /// <summary>
        /// Gets or sets current ship type in the game.
        /// </summary>
        int ShipType { get; set; }

        /// <summary>
        /// Gets or sets current ship level in the game.
        /// </summary>
        int ShipLvl { get; set; }

        /// <summary>
        /// Gets current Hp in the game.
        /// </summary>
        public List<IGameItem> Hp { get; }

        /// <summary>
        /// Gets or sets higscores in the game.
        /// </summary>
        public List<HighScore> HighScores { get; set; }

        /// <summary>
        /// Gets or sets current bullet entity in the game.
        /// </summary>
        public IGameItem Bullet { get; set; }

        /// <summary>
        /// Gets or sets current ship entity in the game.
        /// </summary>
        public IGameItem Ship { get; set; }

        /// <summary>
        /// Gets or sets current asteroids in the game.
        /// </summary>
        public List<IGameItem> Asteroids { get; set; }

        /// <summary>
        /// Gets or sets current powerups in the game.
        /// </summary>
        public List<IGameItem> PowerUps { get; set; }
    }
}
