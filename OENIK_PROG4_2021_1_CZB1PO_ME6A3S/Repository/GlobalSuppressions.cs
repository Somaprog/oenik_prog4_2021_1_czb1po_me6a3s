﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("", "CA1014", Justification ="<NikGitStats>", Scope = "module")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Gitstat>", Scope = "member", Target = "~N:SpaceGameRepository")]
[assembly: SuppressMessage("Reliability", "CA2000:Dispose objects before losing scope", Justification = "<GitStats>", Scope = "namespaceanddescendants", Target = "~N:SpaceGameRepository")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "<NikGitStats>", Scope = "namespaceanddescendants", Target = "~N:SpaceGameRepository")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<NikGitStats>", Scope = "member", Target = "~M:SpaceGameRepository.GameRepository.HighScoreSave(System.String,System.Int32,System.Collections.Generic.List{SpaceGameModel.HighScore})")]
