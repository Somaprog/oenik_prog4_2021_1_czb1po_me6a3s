﻿// <copyright file="IGameRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SpaceGameRepository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using SpaceGameModel;

    /// <summary>
    /// Repo interface.
    /// </summary>
    public interface IGameRepository
    {
        /// <summary>
        /// Save game to db.
        /// </summary>
        /// <param name="info">current gameinfo.</param>
        public void SaveGame(GameInfo info);

        /// <summary>
        /// Load game from db.
        /// </summary>
        /// <returns>Game save entity, to be loaded into model.</returns>
        public GameInfo LoadGame();

        /// <summary>
        /// Delete current saved game from db.
        /// </summary>
        public void DeleteGame();

        /// <summary>
        /// Save highscore to db.
        /// </summary>
        /// <param name="name">Name to be saved.</param>
        /// <param name="score">Score to be saved.</param>
        /// <param name="processedScores">Score list be saved.</param>
        public void HighScoreSave(string name, int score, List<HighScore> processedScores);

        /// <summary>
        /// Load highscores from db.
        /// </summary>
        /// <returns>Highscores organized.</returns>
        public List<HighScore> LoadHighScore();
    }
}
