﻿// <copyright file="GameRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SpaceGameRepository
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Runtime.Serialization.Json;
    using System.Text;
    using System.Text.Json;
    using System.Threading.Tasks;
    using SpaceGameModel;

    /// <summary>
    /// Repo interface.
    /// </summary>
    public class GameRepository : IGameRepository
    {
        /// <summary>
        /// Delete current saved game from db.
        /// </summary>
        public void DeleteGame()
        {
            File.Delete($"../../../../Saves/SaveGame.json");
        }

        /// <summary>
        /// Save highscore to db.
        /// </summary>
        /// <param name="name">Name to be saved.</param>
        /// <param name="score">Score to be saved.</param>
        /// <param name="processedScores">Score list be saved.</param>
        public void HighScoreSave(string name, int score, List<HighScore> processedScores)
        {
            HighScore currScore = new () { Name = name, Score = score };
            var path = $"../../../../Saves/HighScores.json";
            if (processedScores.Count >= 9)
            {
                processedScores.Remove(processedScores.Last());
            }

            processedScores.Add(currScore);
            IOrderedEnumerable<HighScore> oprocessedScores = processedScores.OrderByDescending(x => x.Score);
            processedScores = oprocessedScores.ToList();
            StreamWriter sw = new (path);
            sw.WriteLine(JsonSerializer.Serialize(processedScores));
            sw.Close();
        }

        /// <summary>
        /// Load game from db.
        /// </summary>
        /// <returns>Game save entity, to be loaded into model.</returns>
        public GameInfo LoadGame()
        {
            var path = $"../../../../Saves/SaveGame.json";
            StreamReader sr = new (path);
            var file = sr.ReadToEnd();
            GameInfo processedSave = (GameInfo)JsonSerializer.Deserialize(file, typeof(GameInfo));
            sr.Close();
            return processedSave;
        }

        /// <inheritdoc/>
        public List<HighScore> LoadHighScore()
        {
            var path = $"../../../../Saves/HighScores.json";
            StreamReader sr = new (path);
            var file = sr.ReadToEnd();
            List<HighScore> processedScores = (List<HighScore>)JsonSerializer.Deserialize(file, typeof(List<HighScore>));
            sr.Close();
            return processedScores;
        }

        /// <summary>
        /// Save game to db.
        /// </summary>
        /// <param name="info">current gameinfo.</param>
        public void SaveGame(GameInfo info)
        {
            var path = $"../../../../Saves/SaveGame.json";
            StreamWriter sr = new (path);
            sr.Write(JsonSerializer.Serialize(info));
            sr.Close();
        }
    }
}
