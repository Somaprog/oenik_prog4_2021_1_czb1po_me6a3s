﻿// <copyright file="LogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SpaceGameLogicTests
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.Serialization.Json;
    using System.Text;
    using System.Text.Json;
    using System.Threading.Tasks;
    using Moq;
    using NUnit.Framework;
    using SpaceGameLogic;
    using SpaceGameModel;
    using SpaceGameRepository;

    /// <summary>
    /// Test class for logic.
    /// </summary>
    [TestFixture]
    public class LogicTests
    {
        private GameLogic testLogic;
        private GameRepository testRepo;
        private Mock<IGameRepository> moqRepo;
        private GameModel testModel;

        /// <summary>
        /// Sets up the test.
        /// </summary>
        [SetUp]
        public void SetUpUnitTests()
        {
            this.testRepo = new GameRepository();
            this.moqRepo = new Mock<IGameRepository>();
            this.testModel = new GameModel(10, 10) { Score = 100, CoinInventory = 10, CurrLvl = 1, CurrPup = -1, ShipType = 1, ShipLvl = 2 };
            this.testLogic = new GameLogic(this.testModel, this.testRepo);
        }

        /// <summary>
        /// Testing ctor.
        /// </summary>
        [TestCase]
        public void GameLogicTest()
        {
            IGameLogic logic = new GameLogic(this.testModel);
            Assert.IsInstanceOf(typeof(GameLogic), logic);
        }

        /// <summary>
        /// Testing ctor2.
        /// </summary>
        [Test]
        public void GameLogicTest1()
        {
            IGameLogic logic = new GameLogic(this.testModel, this.moqRepo.Object);
            Assert.IsInstanceOf(typeof(GameLogic), logic);
        }

        /// <summary>
        /// Testing AutoSave.
        /// </summary>
        [Test]
        public void AutoSaveTest()
        {
            this.testLogic.AutoSave();
            var path = $"../../../../Saves/SaveGame.json";
            StreamReader sr = new (path);
            var file = sr.ReadToEnd();
            GameInfo result = (GameInfo)JsonSerializer.Deserialize(file, typeof(GameInfo));
            GameInfo expected = new (100, 10, 1, -1, 1, 2, 3);
            Assert.That(result.GetHashCode(), Is.EqualTo(expected.GetHashCode()));
        }

        /// <summary>
        /// Testing ChangeLvl.
        /// </summary>
        [Test]
        public void ChangeLvlTest()
        {
            int expected = this.testModel.CurrLvl;
            this.testLogic.ChangeLvl();
            int result = this.testModel.CurrLvl;
            Assert.That(expected, Is.EqualTo(result - 1));
        }

        /// <summary>
        /// Testing ChangeLvl.
        /// </summary>
        [Test]
        public void LoadHighScoresTest()
        {
            this.testLogic.LoadHighscores();
            Assert.That(this.testModel.HighScores.Any());
        }

        /// <summary>
        /// Testing ChooseShip.
        /// </summary>
        [Test]
        public void ChooseShipTest()
        {
            int expected = 2;
            this.testLogic.ChooseShip(2);
            int result = this.testModel.ShipType;
            Assert.That(expected, Is.EqualTo(result));
        }

        /// <summary>
        /// Testing CoinUp.
        /// </summary>
        [Test]
        public void CoinUpTest()
        {
            int expected = this.testModel.CoinInventory + 5;
            this.testLogic.CoinUp(5);
            int result = this.testModel.CoinInventory;
            Assert.That(expected, Is.EqualTo(result));
        }

        /// <summary>
        /// Testing Collided.
        /// </summary>
        [Test]
        public void CollidedTest()
        {
            int expected = this.testModel.Hp.Count - 1;
            this.testLogic.Collided();
            int result = this.testModel.Hp.Count;
            Assert.That(expected, Is.EqualTo(result));
        }

        /// <summary>
        /// Testing DeleteSave.
        /// </summary>
        [Test]
        public void DeleteSaveTest()
        {
            this.testLogic.DeleteSave();
            Assert.That(!File.Exists($"{(object)AppDomain.CurrentDomain.BaseDirectory}Saves/SaveGame.json"));
        }

        /// <summary>
        /// Testing GameOver.
        /// </summary>
        [Test]
        public void GameOverTest()
        {
            Assert.That(!File.Exists($"../../../../Saves/SaveGame.json"));
        }

        /// <summary>
        /// Testing HpUp.
        /// </summary>
        [Test]
        public void HpUpTest()
        {
            int expected = this.testModel.Hp.Count;
            this.testLogic.HpUp();
            int result = this.testModel.Hp.Count;
            if (expected == 3)
            {
                Assert.That(expected, Is.EqualTo(result));
            }
            else
            {
                Assert.That(expected + 1, Is.EqualTo(result));
            }
        }

        /// <summary>
        /// Testing Move.
        /// </summary>
        [Test]
        public void MoveTest()
        {
            Tuple<double, double> expected = new (5, 10);

            this.testLogic.MoveShip(5, 10);
            Tuple<double, double> result = new (this.testModel.Ship.CX, this.testModel.Ship.CY);
            Assert.That(expected, Is.EqualTo(result));
        }

        /// <summary>
        /// Testing PickUp.
        /// </summary>
        [Test]
        public void PickPUpTest()
        {
            int expected = 2;
            this.testLogic.PickPUp(2);
            int result = this.testModel.CurrPup;
            Assert.That(expected, Is.EqualTo(result));
        }

        /// <summary>
        /// Testing ScoreUp.
        /// </summary>
        [Test]
        public void ScoreUpTest()
        {
            int expected = this.testModel.Score + 5;
            this.testLogic.ScoreUp(5);
            int result = this.testModel.Score;
            Assert.That(expected, Is.EqualTo(result));
        }

        /// <summary>
        /// Testing ShipUp.
        /// </summary>
        [Test]
        public void ShipUpTest()
        {
            int expected = this.testModel.ShipLvl;
            this.testLogic.ShipUp();
            int result = this.testModel.ShipLvl;
            Assert.That(expected, Is.EqualTo(result - 1));
        }

        /// <summary>
        /// Testing UsePowerUp.
        /// </summary>
        [Test]
        public void UsePowerUpTest()
        {
            this.testModel.CurrPup = 2;
            this.testLogic.UsePowerUp();
            int result = this.testModel.CurrPup;
            Assert.That(result, Is.EqualTo(-1));
        }
    }
}