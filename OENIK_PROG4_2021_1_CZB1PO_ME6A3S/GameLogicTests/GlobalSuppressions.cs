﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("", "CA1014", Justification ="<NikGitStats>", Scope = "module")]
[assembly: SuppressMessage("CodeQuality", "IDE0052:Remove unread private members", Justification = "<NikGitStats>", Scope = "namespaceanddescendants", Target = "~N:SpaceGameLogicTests")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<NikGitStats>", Scope = "namespaceanddescendants", Target = "~N:SpaceGameLogicTests")]
[assembly: SuppressMessage("Reliability", "CA2000:Dispose objects before losing scope", Justification = "<NIKGITSTATS>", Scope = "namespaceanddescendants", Target = "~N:SpaceGameLogicTests")]
